<?php

namespace common\models;
use Yii;
use yii\base\Model;

/**
 * HouseForm represent all data incoming/oucoming w/ api houses
 *
 * @author Ghislain KPOMASSE <coder@ghislainkpomasse.com>
 */
class House extends Model{
    /**
     * properties declaration
     */
    public $referenceNumber;
    public $titel;
    public $description;
    public $shower;
    public $price;
    public $currency;
    public $paymentMode;
    public $housingType;
    public $roomCount;
    public $roomSize;
    public $town;
    public $zone;
    public $radius;
    public $additionalInfo;
    public $rentDeposit;
    public $electricityDeposit;
    public $electricityCounterType;
    public $pictures;
    public $disponibility;
    public $active;
    
    //const declaration
    /**
     * @var string types of shower
     */
    const shower_types = [
        [
            'key' => 'interne',
            'value' => 'Interne'
        ],
        [
            'key' => 'externe',
            'value' => 'Externe'
        ]             
    ];
    
    /**
     * @var string types of payment
     */
    const payment_modes = [
        [
            'key' => 'jour',
            'value' => 'Jour'
        ],
        [
            'key' => 'semaine',
            'value' => 'Semaine'
        ],
        [
            'key' => 'mois',
            'value' => 'Mois'
        ]        
    ];
    
    /**
     * @var string electricity counter type
     */
    const electricity_counter_types = [
        [
            'key' => 'à carte',
            'value' => 'A carte'
        ],
        [
            'key' => 'normal',
            'value' => 'Normal'
        ],        
    ];
    
    /**
     * @var string available
     */
    const HOUSE_AVAILABLE = "libre";
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // email has to be a valid email address
            ['titel', 'string', 'min' => 5, 'max' => 120],
            ['currency', 'string'],
            ['price', 'double'],
            [['pictures'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 4],
            [['titel', 'referenceNumber', 'description', 'shower', 'price', 'currency', 'paymentMode', 'housingType', 'roomCount', 'roomSize', 'radius', 'town', 'zone', 'additionalInfo', 'rentDeposit', 'electricityDeposit', 'electricityCounterType', 'disponibility', 'active'], 'safe']
        ];
    }       
}
