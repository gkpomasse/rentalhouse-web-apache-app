<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\Core;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    /**
     * translated label names
     * @return array
     */
    public function attributeLabels() {
        /*return [
            'username' => Yii::t('app', 'username'),
            'password' => Yii::t('app', 'password'),
            'rememberMe' => Yii::t('app', 'rememberMe')
        ]; *///review translation later 
        return [
            'username' => "Nom d'utilisateur",
            'password' => "Mot de Passe",
            'rememberMe' => "Se souvenir de Moi"
        ];         
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            $roles = $user['roles'];
            /**
             * then roles not empty from api to know if success logged through api or not
             */
            if (empty($roles)) {
                $this->addError($attribute, 'Veuillez vérifier votre nom d\'utilisateur et votre mot de passe!');
            } else {
                if(!in_array(Yii::$app->params['allowedUserRole'], $roles)) {//check if user have right role to access
                    $this->addError($attribute, 'Vous ne disposez pas des droits requises pour accéder à cette section!');
                } else{} //nothing will happen!!!               
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {//all validations from api have been added to validatePassword() method           
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findWithApi($this->username, $this->password);
        }

        return $this->_user;
    }
}
