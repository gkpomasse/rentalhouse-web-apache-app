<?php
namespace common\components;

use Yii;
use linslin\yii2\curl;
/**
 * Created by PhpStorm.
 * User: Ghislain KPOMASSE <gkpomasse@gmail.com>
 * Date: 24/03/2018
 * Time: 00:43
 */

class ApiHelper
{
    /**
     * @var string api url
     */
    const API_URL = 'http://80.211.167.45:8080/';
    /**
     * @var string 
     */
    const HOST_URL = 'https://www.beninimmobilier.com/';
    /**
     * @var string
     */
    const HOUSE_UPLOAD_PATH = 'uploads/houses';
    /**
     * @var string
     */
    const USER_UPLOAD_PATH = 'uploads/users';
    /**
     * @var load dropdowns values inside search api
     */
    const SEARCH_QUESTION_URL = 'maison-a-louer/api/panel/search/questions';
    /**
     * @var string main houses api
     */
    const LIST_HOUSES_URL = 'maison-a-louer/api/houses';
    /**
     * @var string search houses api url
     */
    const SEARCH_HOUSE_URL = 'maison-a-louer/api/panel/search/data';
    /**
     * @var integer redis caching one day expiry value
     */
    const REDIS_ONE_DAY_EXPIRY = 86400;
    /**
     * @var integer redis caching one hour expiry value
     */
    const REDIS_ONE_HOUR_EXPIRY = 3600;

    //search panel constants
    /**
     * @var string housingtype
     */
    const SEARCH_PANEL_HOUSETYPE = "search.panel.housingtype";
    /**
     * @var string room_count
     */
    const SEARCH_PANEL_ROOMCOUNT = "search.panel.room_count";
    /**
     * @var string town
     */
    const SEARCH_PANEL_TOWN = "search.panel.town";
    /**
     * @var string zone
     */
    const SEARCH_PANEL_ZONE = "search.panel.zone";
    /**
     * @var string price_min
     */
    const SEARCH_PANEL_PRICE_MIN = "search.panel.price_min";
    /**
     * @var string price_max
     */
    const SEARCH_PANEL_PRICE_MAX = "search.panel.price_max";
    /**
     * @var string surface_min
     */
    const SEARCH_PANEL_SURFACE_MIN = "search.panel.surface_min";
    /**
     * @var string surface_max
     */
    const SEARCH_PANEL_SURFACE_MAX = "search.panel.surface_max";
    /**
     * @var string publication_date
     */
    const SEARCH_PANEL_PUBLICATION_DATE = "search.panel.publication_date";
    /**
     * @var string radius panel search
     */
    const SEARCH_PANEL_RAYON = "search.panel.radius";
    /**
     * @var integer limit houses per page in search area
     */
    const SEARCH_HOUSES_PER_PAGE = 10;
    /**
     * @var integer limit houses into houses pages
     */
    const HOUSES_PER_PAGE = 10;
    /**
     * @var integer default api pagination number
     */
    const DEFAULT_PAGE_NUMBER = 0;
    /**
     * @var string houses api link w/ paginations included
     */
    const LIST_HOUSE_WITH_PAGINATION_URL = 'maison-a-louer/api/houses/pagination';
    /**
     * @var string categories property count api
     */
    const HOUSING_TYPE_PROPERTY_COUNT = 'maison-a-louer/api/house/housingType/count';
    /**
     * @var string
     */
    const LIST_VIEW_MODE = 'listview';
    /**
     * @var string
     */
    const GRID_VIEW_MODE = 'gridview';
    /**
     * @var string
     */
    const VIEW_HOUSE_BASE_URL = 'maison-a-louer/api/house';
    /**
     * @var connexion link
     */
    const CONNEXION_LINK = 'maison-a-louer/login';
    /**
     * @var signup link
     */
    const SIGNUP_LINK = 'maison-a-louer/api/create/user';
    /**
     * @var get authenticated user url
     */
    const AUTHENTICATED_USER_URL = 'maison-a-louer/api/authenticated/user';
    /**
     * @var string
     */
    const LOGOUT_URL = 'maison-a-louer/logout';
    /**
     * @var string
     */
    const LIST_ARCHIVED_HOUSES = 'maison-a-louer/api/archived/houses';
    /**
     * @var string
     */
    const DELETE_HOUSE_URL = 'maison-a-louer/api/delete/house';

    /**
     * @return array
     * //@todo constant for others also
     */

    public static function getFullQuestionsList() {
        return [
            "questionIds" => [
                self::SEARCH_PANEL_HOUSETYPE,
                self::SEARCH_PANEL_ROOMCOUNT,
                self::SEARCH_PANEL_TOWN,
                self::SEARCH_PANEL_ZONE,
                self::SEARCH_PANEL_PRICE_MIN,
                self::SEARCH_PANEL_SURFACE_MAX,
                self::SEARCH_PANEL_SURFACE_MIN,
                self::SEARCH_PANEL_SURFACE_MAX,
                self::SEARCH_PANEL_PUBLICATION_DATE,
                self::SEARCH_PANEL_RAYON
            ]
        ];
    }
    
    /**
     * housing type only!!!
     * @return type
     */
    public static function getHousingTypeOnlyQuestions() {
        return [
            "questionIds" => [
                self::SEARCH_PANEL_HOUSETYPE
            ]
        ];        
    }
    
    /**
     * @return array list of towns
     */
    public static function getTownOnlyQuestions() {
        return [
            "questionIds" => [
                self::SEARCH_PANEL_TOWN
            ]
        ];         
    }
    
    /**
     * 
     * @return array list of zones
     */
    public static function getZoneOnlyQuestions() {
        return [
            "questionIds" => [
                self::SEARCH_PANEL_ZONE
            ]
        ];         
    }
    
    /**
     * 
     * @return array list of radius
     */
    public static function getRadiusOnlyQuestions() {
        return [
            "questionIds" => [
                self::SEARCH_PANEL_RAYON
            ]
        ];         
    }
    
    /**
     * @param type $question
     * @return type
     */
    public static function loadQuestionParams($question = '') {
        switch($question) {
            case self::SEARCH_PANEL_HOUSETYPE:
                $params = self::getHousingTypeOnlyQuestions();
                break;
            case self::SEARCH_PANEL_TOWN:
                $params = self::getTownOnlyQuestions();
                break;
            case self::SEARCH_PANEL_ZONE:
                $params = self::getZoneOnlyQuestions();
                break;
            case self::SEARCH_PANEL_RAYON:
                $params = self::getRadiusOnlyQuestions();
                break;
            default :
                $params = self::getFullQuestionsList();
                break;
        }
        return $params;
    }
    
    /**
     * 
     * @param type $response
     */
    public static function runActionFromResponse($response) {
        switch($response) {
            case 403: //access denied
                $homeUrl = \yii\helpers\Url::to(['site/login']);
                Yii::$app->user->logout();
                Yii::$app->getResponse()->redirect($homeUrl)->send();
                break;
        }
    }

    /**
     * 
     * @param string $question
     * @return array
     * 
     */
    public static function loadSingleQuestions($question = '') {
        $curl = new curl\Curl();
        $params = self::loadQuestionParams($question);
        $encodedParams = json_encode($params);
        $response = $curl->setRequestBody($encodedParams)
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($encodedParams)
            ])
            ->post(self::API_URL . self::SEARCH_QUESTION_URL);

        $questions = json_decode($response, true);
        return (!empty($questions['_embedded']["questions"])) ? $questions['_embedded']["questions"][0]['options'] : [];        
    }

    /**
     * @return array
     */
    public static function getHomeResultsFilterOptions() {
        return [
            "SORT_BY_DATE" => Yii::t('app', 'Par date'),
            "SORT_BY_PRICE" => Yii::t('app', 'Par prix'),
            "SORT_BY_SURFACE" => Yii::t('app', 'Par surface'),
            "SORT_BY_TOWN" => Yii::t('app', 'Par ville')
        ];
    }

    /**
     * @return array
     */
    public static function getHousingTypeLabel()
    {
        $data = self::buildSearchData();
        $output = [];
        foreach ($data['housingTypeOptions'] as $option) {
            $output[$option['key']] = $option['value'];
        }
        return $output;
    }

    /**
     * @param $result
     * @return bool
     */
    public static function searchHousingType($result) {
        return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_HOUSETYPE);
    }

    /**
     * @param $result
     * @return bool
     */
    public static function searchRoomCount($result) {
        return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_ROOMCOUNT);
    }

    /**
     * @param $result
     * @return bool
     */
    public static function searchTown($result) {
        return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_TOWN);
    }

    /**
     * @return array
     */
    public static function getRecentPropertyParams() {
        return [
            'pageNr' => 0,
            'pageSize' => 6,//no housing type here
            'sortBy' => 'SORT_BY_DATE'
        ];
    }

    /**
     * @return array
     */
    public static function getCategoryPropertyParams() {
        return [
            'pageNr' => 0,
            'pageSize' => self::HOUSES_PER_PAGE
        ];
    }

    /**
     * @param $token
     * @return array
     */
    public static function findUser($token) {
        $curl = new curl\Curl();
        $params = [];//@todo: can be extended in future if something needed!
        $response = $curl->setGetParams($params)
            ->setHeaders([
                'X-Auth-Token' => $token,
            ])
            ->get(ApiHelper::API_URL . ApiHelper::AUTHENTICATED_USER_URL);
        //get result
        $result = json_decode($response, true);
        //remove links from array
        unset($result['_links']);
        return (empty($result)) ? [] : $result;
    }

    /**
     * @param $username
     * @param $password
     * @return array|bool
     */
    public static function login($username, $password)
    {
        $params = [
            "username" => $username,
            "password" => $password
        ];
        //
        $curl = new curl\Curl();
        $encodedParams = json_encode($params);
        //var_dump($encodedParams);
        $result = $curl->setRequestBody($encodedParams)
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($encodedParams)
            ])
            ->post(ApiHelper::API_URL . ApiHelper::CONNEXION_LINK);
        //var_dump($result);exit();
        $headers = $curl->responseHeaders;
        return (!empty($headers['X-Auth-Token'])) ? ['X-Auth-Token'=>$headers['X-Auth-Token']] : false;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public static function signup($params) {
        $curl = new curl\Curl();
        
        $boundary = hash('sha256', uniqid('', true));

        $encodedParams = json_encode($params); 
        
        $postdata = "--" . $boundary . "\r\n";
        $postdata .= "Content-Type: application/json; charset=\"UTF-8\";\r\n";
        $postdata .= "Content-Disposition: form-data; name=\"data\"\r\n\r\n";
        $postdata .= $encodedParams;
        $postdata .= "\r\n\r\n";
        //
        $postdata .= "--" . $boundary . "\r\n";
        $postdata .= "Content-Type: application/octet-stream;\r\n";
        $postdata .= "Content-Disposition: form-data; r\n\r\n";
        $postdata .= "\r\n\r\n";
        $postdata .= "--" . $boundary . "--\r\n";
        
        $response = $curl->setRequestBody($postdata)
            ->setHeaders([
                'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
            ])
            ->post(self::API_URL . self::SIGNUP_LINK);
        
        $data = json_decode($response, true);
        //log user
        $logUser = self::login($params['telephoneNr'], $params['password']);
        $findUser = \common\components\ApiHelper::findUser($logUser['X-Auth-Token']);
        //setup user session
        $session = Yii::$app->session;
        $_SESSION['currentUser'] = $findUser;
        $_SESSION['currentUser']['token'] = $logUser['X-Auth-Token'];
        
        return (!empty($data['content']["user creation failed: "])) ? false : $findUser;
    }

    public static function logout() {
        if(!empty($_SESSION['currentUser']['token'])) {//make sure for valid token
            $curl = new curl\Curl();
            $params = [];//@todo: can be extended in future if something needed!
            $response = $curl->setGetParams($params)
                ->setHeaders([
                    'X-Auth-Token' => $_SESSION['currentUser']['token'],
                ])
                ->get(ApiHelper::API_URL . ApiHelper::LOGOUT_URL);
        }
    }

    /**
     * @return array
     */
    public static function defaultAllPropertiesParams() {
        return [
            'pageNr' => 0,
            'pageSize' => self::HOUSES_PER_PAGE
        ];
    }

    /**
     * retrieve recent properties
     * @return array|mixed
     */
    public static function getLatestProperties() {
        return self::getProperties(self::getRecentPropertyParams());
    }

    /**
     * retrieve all properties w/ paginations allowed
     * @param array $params
     * @return mixed
     */
    public static function getAllProperties($params = [], $pagination = true) {
        return (empty($params)) ? self::getProperties(self::defaultAllPropertiesParams(), $pagination) : self::getProperties($params, $pagination);
    }
    
    /**
     * retrieve all archived properties
     */
    public static function getArchiveProperties() {
        $curl = new curl\Curl();
        $params = [];
        $response = $curl->setGetParams($params)
                        ->setHeaders([
                        'X-Auth-Token' => \Yii::$app->user->identity->token,
                    ])->get(ApiHelper::API_URL . ApiHelper::LIST_ARCHIVED_HOUSES);
        //var_dump($response, $curl->responseCode);
        //exit();
        //get result
            $houses = json_decode($response, true);
            //self::runActionFromResponse($curl->responseCode);
        //return counts as array
        return (!empty($houses['_embedded'])) ? $houses['_embedded']["houses"] : [];      
    }

    public static function deleteHouse($id) {
        $curl = new curl\Curl();
        $username = \Yii::$app->user->identity->telephoneNr;
        if(!empty($username)) {
            $response = $curl->setHeaders([
                        'X-Auth-Token' => \Yii::$app->user->identity->token,
                    ])->delete(self::API_URL . self::DELETE_HOUSE_URL . '/' . $id . '/' . $username);
            return json_decode($response, true);
        }
    }

    /**
     * Retrive properties
     * @return array|mixed
     */
    public static function getProperties($params, $pagination = true) {
        $curl = new curl\Curl();
        $callUrl = ($pagination) ? ApiHelper::LIST_HOUSE_WITH_PAGINATION_URL : ApiHelper::LIST_HOUSES_URL;
        $encodedParams = json_encode($params);
        $response = $curl->setRequestBody($encodedParams)
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($encodedParams)
            ])
            ->post(ApiHelper::API_URL . $callUrl);

        $houses = json_decode($response, true);
        return (!empty($houses['_embedded'])) ? $houses['_embedded']["houses"] : [];
    }

    /**
     * @return array
     */
    public static function getHousingTypeCount() {
        $curl = new curl\Curl();
        $params = [];
        $response = $curl->setGetParams($params)
            ->get(ApiHelper::API_URL . ApiHelper::HOUSING_TYPE_PROPERTY_COUNT);
        //get result
            $result = json_decode($response, true);
        //return counts as array
        return (empty($result)) ? [] : $result['content'];
    }

    /**
     * @return array
     */
    public static function publicationQuestions() {
        return [
            [
                'key' => "ROW_TODAY",
                'value' => "Aujourd'hui",
                'calendar' => 0
            ],
            [
                'key' => "ROW_THISWEEK",
                'value' => "Cette semaine",
                'calendar' => 0
            ],
            [
                'key' => "ROW_THISMONTH",
                'value' => "Ce mois",
                'calendar' => 0
            ],
           /* [
                'key' => "ROW_CUSTOM",
                'value' => "Personnaliser",
                'calendar' => 1
            ]*/
        ];
    }

    /**
     * call api for view specific property
     * @param $reference
     * @return array
     */
    public static function viewProperty($reference) {
        $curl = new curl\Curl();
        $params = [];
        $response = $curl->get(ApiHelper::API_URL . ApiHelper::VIEW_HOUSE_BASE_URL . SYS_DIRECTORY_SEPARATOR . $reference);
        //get result
        $result = json_decode($response, true);
        //var_dump(ApiHelper::API_URL . ApiHelper::VIEW_HOUSE_BASE_URL . SYS_DIRECTORY_SEPARATOR . $reference);exit();
        //return counts as array
        return (empty($result)) ? [] : $result;
    }

    /**
     * @return array|mixed
     */
    public static function buildSearchData() {
        /**
         * Process :
         * 1/ check if questions already stored in redis
         * 2/ If not yet, call api and decode values
         * 3/ return decoded to view for fill dropdowns
         */
        $cache = Yii::$app->cache;
        $key   = '__KEY__Questions__';
        $response  = $cache->get($key);
        //var_dump("cache status", $response);
        //if ($response === false) {//1/
            //2/
            $curl = new curl\Curl();
            $encodedQuestion = json_encode(self::getFullQuestionsList());

            $response = $curl->setRequestBody($encodedQuestion)
                ->setHeaders([
                    'Content-Type' => 'application/json',
                    'Content-Length' => strlen($encodedQuestion)
                ])
                ->post(self::API_URL . self::SEARCH_QUESTION_URL);
            //var_dump($response);
            //
            if ($curl->errorCode === null) {
                //3 store cache
                $cache->set($key, $response, ApiHelper::REDIS_ONE_HOUR_EXPIRY);
            } else {//load core app settings

            }
        //}

        $data = json_decode($response, true);

        if(!empty($data)) {
            $questions = $data['_embedded']['questions'];
            /*echo '<pre>';
            print_r($questions);
            echo '</pre>';*/
            $housingType = array_filter($questions, function ($result) {
                return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_HOUSETYPE);
            });
            $housingTypeOptions = $housingType[0]['options'];
            //
            $rowsCount = array_filter($questions, function ($result) {
                return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_ROOMCOUNT);
            });
            
            //exit();
            $rowsCountOptions = $rowsCount[1]['options'];
            //
            $towns = array_filter($questions, function ($result) {
                return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_TOWN);
            });
            $townOptions = $towns[2]['options'];
            //
            $rowsZone = array_filter($questions, function ($result) {
                return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_ZONE);
            });
            $rowsZoneOptions = $rowsZone[3]['options'];
            //
            //$rowspublicationOptions = ApiHelper::publicationQuestions();
            $rowspublication = array_filter($questions, function ($result) {
                return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_PUBLICATION_DATE);
            });
            $rowspublication = array_shift($rowspublication);
            //var_dump($rowspublication);exit();
            $rowspublicationOptions = $rowspublication['options'];
            //radius
            $rowsRadius =array_filter($questions, function ($result) {
                return (is_array($result) && $result['questionId'] == ApiHelper::SEARCH_PANEL_RAYON);
            });
            $rowsRadius = array_shift($rowsRadius);
            $rowsRadiusOptions = $rowsRadius['options'];

            //build data
            $data = [
                'housingTypeOptions' => $housingTypeOptions,
                'rowsCountOptions' => $rowsCountOptions,
                'rowsZoneOptions' => $rowsZoneOptions,
                'townOptions' => $townOptions,
                'rowspublicationOptions' => $rowspublicationOptions,
                'rowsRadiusOptions' => $rowsRadiusOptions
            ];
        } else {
            $data = [];
        }
        return $data;
    }

    /**
     * @return array
     */
    public static function categoryPictures() {
        return [
            'images/index-4.jpg',
            'images/index-1.jpg',
            'images/index-5.jpg',
            'images/index-2.jpg',
            'images/index-10.jpg'
        ];
    }

    /**
     * @return array
     */
    public static function resultDisplayMode() {
        return [
            self::GRID_VIEW_MODE,
            self::LIST_VIEW_MODE
        ];
    }
    
    /**
     * 
     * @param array $params
     * @return array houses found through api
     */
    public static function searchHouses($params) {
        //call api with these paramaters
        $curl = new curl\Curl();
        $searchQuery = json_encode($params);
        //
        $response = $curl->setRequestBody($searchQuery)
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($searchQuery)
            ])
            ->post(ApiHelper::API_URL . ApiHelper::SEARCH_HOUSE_URL);

        $data = json_decode($response, true);
        //
        return (!empty($data['_embedded'])) ? $data['_embedded']['houses'] : [];        
    }
}
