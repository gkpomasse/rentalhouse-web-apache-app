<?php
namespace common\components;

use Yii;
use yii\base\ActionFilter;

/**
 * Description of UserApiLoggedFilter
 *
 * @author Ghislain KPOMASSE <coder@ghislainkpomasse.com>
 */
class UserApiLoggedFilter extends ActionFilter {
    /**
     * making sure that user is still authenticated at api side
     * @param type $action
     * @return type
     */
    public function beforeAction($action)
    {
        //$session = Yii::$app->session;
        //var_dump(Yii::$app->user->identity->token);
        if(!Yii::$app->user->isGuest && empty(Yii::$app->user->identity->token)) {
            $homeUrl = \yii\helpers\Url::to(['site/login']);
            Yii::$app->user->logout();
            //Yii::$app->getResponse()->redirect($homeUrl)->send();
        }        
        return parent::beforeAction($action);
    }
    
    /**
     * nothing there for the moment
     * @param type $action
     * @param type $result
     * @return type
     */
    public function afterAction($action, $result)
    {
        return parent::afterAction($action, $result);
    }    
}