<?php

namespace common\components;

/**
 * All shared properties for backend/frontend
 *
 * @author Ghislain KPOMASSE
 */
class Core {
    /**
     * @var string
     */
    const USER_ROLE_ADMIN = 'ROLE_ADMIN';
    
    /**
     * @var string
     */
    const USER_ROLE_USER = 'ROLE_USER';
}
