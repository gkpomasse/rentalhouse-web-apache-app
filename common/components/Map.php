<?php

/*
 * @author Ghislain KPOMASSE <gkpomasse@gmail.com>
 */

namespace common\components;


class Map {
    public static function decodeAddress($address) {
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyBgiEI2e8TGb1cjNXGUYuHdTP2Gd98UVTQ');
        $output= json_decode($geocode);
        //var_dump($output);
        //exit();
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
        return [
            'latitude' => $latitude,
            'longitude' => $longitude
        ];
    }
}
