var jsBaseUrl = 'http://80.211.167.45:8080/';
window.house = window.house || {};

window.house.exec = function (method, url, data) {
    var dfd = $.Deferred();
    var response = {};
    $.ajax({
        url: url,
        method: method,
        data: data,
        dataType: "json",
        cache: true,
        timeout: 6000000,
        beforeSend: function () {
            //can show loading icon here
            $('#loading').show();
        },
        success: function (msg) {
            response = msg;
        },
        complete: function (jqXHR, textStatus) {
            dfd.resolve(response);
            $('#loading').hide();
        },
        failure: function (transport) {
            dfd.reject({
                status: 'fail'
            });
        },
        exception: function (transport) {
            dfd.reject({
                status: 'fail'
            });
        }
    });
    return dfd.promise();
};

$('.deleteHouse').unbind('click').bind('click', function(e) {
    e.preventDefault();
    let $el = $(this);
    if(confirm("Voulez vous vraiment supprimer cet élément ?") == true) {
        var id = $(this).attr('data-ref');//reference
        var username = $(this).closest('tr').attr('data-id');
        var url = 'delete/?id=' + id;
        window.house.exec('get', url, {}).then(function (result) {
            $el.closest('tr').remove();
            new PNotify(notification = {
                title: 'Notification',
                text: result.message,
                type: result.status
            });
        });
    }
});