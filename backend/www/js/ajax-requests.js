/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).on('click', '#list-active', function() {
    $('#tab-available').click();
});

$(document).on('click', '#list-unactive', function() {
    $('#tab-awaiting').click();
});

$(document).on('click', '#tab-home', function() {
    window.houses.list("");
});

$(document).on('click', '#tab-available', function() {
    window.houses.list("active");
});

$(document).on('click', '#tab-awaiting', function() {
    window.houses.list("unactive");
});

$(document).on('click', '#tab-archive', function() {
    window.houses.list("deleted");
});

window.houses = window.houses || {};
window.houses.list = function (filter) {
    var url = 'http://80.211.167.45:8082/maison-a-louer/api/houses';
    var data = {};
    switch(filter) {
        case "active":
            data.active = true;
            break;
        case "unactive":
            data.active = false;
            break;
        case "deleted":
        
            break;
    }
    var dfd = $.Deferred();
    var response = {};
    $.ajax({
        url: url,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            //'Access-Control-Allow-Origin': '*'
        },
        'crossDomain': true,
        dataType: "json",
        cache: true,
        timeout: 6000000,
        beforeSend: function () {
        },
        success: function (msg) {
            response = msg;
            console.log(response);
            //$('#houses-results').html(msg.data);
        },
        complete: function (jqXHR, textStatus) {
            dfd.resolve(response);
        },
        failure: function (transport) {
            dfd.reject({status: 'fail'});
        },
        exception: function (transport) {
            dfd.reject({status: 'fail'});
        }
    });
    return dfd.promise();
};

window.houses.renderHouses = function(data) {
    
};

window.houses.getHousesBySort = function (sortingValue) {
    var dfd = $.Deferred();
    var response = {};
    $.ajax({
        url: 'houses/ajax-load-sorting-result?sortBy=' + sortingValue,
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: "json",
        cache: true,
        timeout: 6000000,
        beforeSend: function () {
        },
        success: function (msg) {
            response = msg;
            $('#houses-results').html(msg.data);
        },
        complete: function (jqXHR, textStatus) {
            dfd.resolve(response);
        },
        failure: function (transport) {
            dfd.reject({status: 'fail'});
        },
        exception: function (transport) {
            dfd.reject({status: 'fail'});
        }
    });
    return dfd.promise();
};