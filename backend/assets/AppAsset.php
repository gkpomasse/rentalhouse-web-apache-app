<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'plugins/bootstrap/css/bootstrap.css',
        'plugins/bootstrap-select/css/bootstrap-select.css',
        'plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css',
        'plugins/node-waves/waves.css',
        'plugins/morrisjs/morris.css',
        'css/themes/all-themes.css',
        'plugins/animate-css/animate.css',
        'plugins/CustomFileInputs/css/component.css',
        //'plugins/CustomFileInputs/css/demo.css',
        //'plugins/CustomFileInputs/css/normalize.css',
        'css/style.css'
    ];
    public $js = [
        'plugins/jquery/jquery.min.js',
        'plugins/bootstrap/js/bootstrap.js',
        'plugins/momentjs/moment.js',
        'plugins/bootstrap-select/js/bootstrap-select.js',
        'plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js',
        'plugins/node-waves/waves.js',
        'plugins/jquery-validation/jquery.validate.js',
        //'plugins/js/admin.js',
        'plugins/autosize/autosize.js',      
        'js/pages/examples/sign-in.js',
        'plugins/jquery-slimscroll/jquery.slimscroll.js',
        'plugins/jquery-countto/jquery.countTo.js',
        'plugins/raphael/raphael.min.js',
        'plugins/morrisjs/morris.js',
        'plugins/chartjs/Chart.bundle.js',
        'plugins/flot-charts/jquery.flot.js',
        'js/pages/forms/basic-form-elements.js',
        /*'plugins/flot-charts/jquery.flot.resize.js',
        'plugins/flot-charts/jquery.flot.pie.js',
        'plugins/flot-charts/jquery.flot.categories.js',
        'plugins/flot-charts/jquery.flot.time.js',
        'plugins/jquery-sparkline/jquery.sparkline.js',*/
        //'js/pages/forms/basic-form-elements.js',
        'js/admin.js',
        'js/pages/index.js',
        'js/demo.js',
        'js/ajax-requests.js',
        'plugins/CustomFileInputs/js/custom-file-input.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
