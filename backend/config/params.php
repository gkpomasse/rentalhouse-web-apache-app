<?php
return [
    'adminEmail' => 'admin@example.com',
    'backOfficeVersion' => '1.0.0',
    'currencies' => [
        [
            'key' => 'FCFA',
            'value' => 'FCFA'
        ],
        [
            'key' => 'Euros',
            'value' => 'Euros'
        ],
        [
            'key' => 'Dollars',
            'value' => 'Dollars'
        ]
    ],
    'allowedUserRole' => 'ROLE_ADMIN'//permet de se connecter en tant qu'administrateur
];
