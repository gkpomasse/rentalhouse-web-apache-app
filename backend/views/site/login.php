<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #login-form .col-lg-3 {
        margin-bottom: 0;
    }
    #login-form input {
        width: 290px;
    }
</style>
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);">Espace<b>Administration</b></a>
    </div>
    <div class="card">
        <div class="body">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                    ],
                ]); ?>
                <div class="msg">Se Connecter</div>
                <?php if(!empty($model->errors)):?>
                <div class="alert alert-warning">
                    <?php foreach($model->errors as $key=>$error):?>
                        <?= implode("<br/> ", $error)?>
                    <?php endforeach;?>
                </div>
                <?php endif;?>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'required' => true, 'class'=>'form-control', 'placeholder'=>"Username"])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <?= Html::activePasswordInput($model, 'password', ['required' => true, 'class'=>'form-control', 'placeholder'=>"Password", 'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>"]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 p-t-5">
                        <?= $form->field($model, 'rememberMe', ['options' => ['class'=>'filled-in chk-col-pink']])->checkbox([
                            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",

                        ]) ?>
                    </div>
                    <div class="col-xs-4">
                        <?= Html::submitButton('Login', ['class' => 'btn btn-block bg-pink waves-effect', 'name' => 'login-button']) ?>
                    </div>                   
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
