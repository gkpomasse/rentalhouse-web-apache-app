<?php
/* @var $this yii\web\View */
$this->title = 'Toutes les Catégories';
?>
       <div class="container-fluid">
            <!--div class="block-header">
                <h2><?=$this->title;?></h2>
            </div-->
            <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?=$this->title;?>
                                <small>Toutes les catégories sont listés dans cette section</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Catégorie</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($models as $key=>$category):?>                                 
                                    <tr>                            
                                        <th scope="row"><?=$key+1;?></th>
                                        <td valign="middle"><?= ucfirst($category['value']);?></td>
                                        <td style="width: 400px">
                                            <a href="javascript:;" class="btn btn-sm btn-info" style="margin-right: 5px"><span><i class="glyphicon glyphicon-eye-open"></i> Consulter</span></a>
                                            <a href="javascript:;" class="btn btn-sm btn-warning" style="margin-right: 5px"><span><i class="glyphicon glyphicon-pencil"></i> Modifier</span></a>
                                            <a href="javascript:;" class="btn btn-sm btn-danger"><span><i class="glyphicon glyphicon-trash"></i> Supprimer</span></a>
                                        </td>                                        
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Table -->