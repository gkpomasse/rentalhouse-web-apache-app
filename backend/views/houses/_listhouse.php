<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ApiHelper;
?>
<style>
    .table .actions {
        width: 200px
    }
    .table .actions a {
        margin-bottom: 5px;
    }
    .table img {
        width:175px;
        height:120px;
        border: 5px solid silver;
        border-radius: 5px;
    }
</style>
<table class="table">
    <thead>
        <tr>
            <th>Ref.</th>
            <th>Photo</th>
            <th>Titre</th>
            <th>Description</th>
            <th>Prix</th>
            <th>Addresse</th>
            <th>Agent</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($houses as $key=>$house):?>
        <?php
            $agent_contact = (!empty($house['contact'])) ? $house['contact'] : [];
            if (strpos($house['description'], ".") !== false) {
                $description = substr($house['description'], 0, strpos($house['description'], '.')) . "...";
            } else {
                $description = $house['description'];
            }
        ?>                                    
        <tr>                            
            <th scope="row"><?=$house['referenceNumber'];?></th>
            <td>
            <?php if(!empty($house['pictures'])):?>
                <img src="<?= ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $house['referenceNumber'];?>/<?= !empty($house['pictures'][0]) ? $house['pictures'][0] : '';?>"/>
            <?php else :?>
                Aucune Image disponible
            <?php endif;?>
            </td>
            <td><?=$house['titel'];?></td>
            <td><?=$description;?></td>
            <td><?=$house['price'];?> <?= Yii::$app->params['localeCurrency'];?>/<?=$house['paymentMode'];?></td>                                        
            <td><?=$house['zone'];?>, <?=$house['town'];?></td>
            <td>
            <?php if(!empty($agent_contact)) : ?>                                           
                <?=$agent_contact['firstName'];?> <?=$agent_contact['surName'];?>
            <?php else:?>
                ---
            <?php endif;?>
            </td>
            <td class="actions">
                <?php if(!empty($deleted) && $deleted) :?>
                    <a class="btn btn-sm btn-primary"><span><i class="glyphicon glyphicon-refresh"></i> Restaurer</span></a>
                <?php else :?>
                    <a href="<?=Url::to(['houses/view', 'id'=>$house['referenceNumber']]);?>" class="btn btn-sm btn-info"><span><i class="glyphicon glyphicon-eye-open"></i> Consulter</span></a>
                    <a href="<?=Url::to(['houses/update', 'id'=>$house['referenceNumber']]);?>" class="btn btn-sm btn-warning"><span><i class="glyphicon glyphicon-pencil"></i> Modifier</span></a>
                    <a class="btn btn-sm btn-danger deleteHouse" data-ref="<?=$house['referenceNumber'];?>"><span><i class="glyphicon glyphicon-trash"></i> Supprimer</span></a>                    
                <?php endif;?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<script src="<?=Url::base()?>/js/houses.js"></script>