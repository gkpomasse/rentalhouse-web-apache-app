<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use common\components\ApiHelper;
    //find all options
    $housingTypes = $data['questions']['housingTypeOptions'];//type de logement
    $towns = $data['questions']['townOptions'];
    /*$defaultTownOption = [
        'key' => '',
        'value' => 'Toutes les villes'
    ];*/
    //add option to begin
    //array_unshift($towns , $defaultTownOption);
    
    $zones = $data['questions']['rowsZoneOptions'];
    /*$defaultZoneOption = [
        'key' => '',
        'value' => 'Toutes les zones'
    ];*/
    //add option to begin
    //array_unshift($zones , $defaultZoneOption);
    
    $rayons = $data['questions']['rowsRadiusOptions'];     
    /*$defaultRayonOption = [
        'key' => '',
        'value' => 'Dans un rayon de'
    ];
    //add option to begin
    array_unshift($rayons , $defaultRayonOption);  */  
    $showerTypes = $data['options']['shower_types'];
    $paymentTypes = $data['options']['payment_modes'];
    $counterTypes = $data['options']['electricity_count_types'];
?>
<style>
    .house-form {
        min-height: 600px;
    }
    
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }    
    
    .remove-picture-button {
        border-radius: 15px;
        position: relative;
        margin-top: -65px;
        margin-left: 10px
    }
    
    .picture-box {
        width:175px;
        height:120px;
        border: 5px solid silver;
        border-radius: 5px;
    }
</style>    
<div class="body table-responsive">
    <?php $form = ActiveForm::begin(['id' => 'form_validation', 'options' => ['class' => 'house-form']]); ?>    
        <div class="row clearfix">
            <div class="col-md-5">
                <div class="form-group" >
                    <div class="form-line" style="border: none;">
                    <label for="select-1"><?= \Yii::t('app', "TYPE DE LOGEMENT")?></label>
                    <?= Html::activeDropDownList($model, 'housingType', ArrayHelper::map($housingTypes, 'key', 'value'), [
                        'prompt'=>'Tous les types de logement', 
                        'id'=>'select-housing-type',
                        'data-selected'=>$model->housingType,
                        'options' => [
                            'class' => 'rd-mailform-select'
                        ]
                    ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line">
                        <?= Html::activeTextInput($model, 'roomCount', ['required'=>true, 'class' => 'form-control']);?>
                        <label class="form-label">Nombre de Pièces</label>
                    </div>
                </div>
            </div>             
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line">
                        <?= Html::activeTextInput($model, 'titel', ['required'=>true, 'class' => 'form-control']);?>
                        <label class="form-label">Titre</label>
                    </div>
                </div>
            </div>
        </div>               
        <div class="row clearfix">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-line">
                        <label class="form-label">Saisissez une description du logement...</label>
                        <?= Html::activeTextarea($model, 'description', ['required'=>true, 'class' => 'form-control', 'rows' => 5, 'cols' => 30]);?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-line">
                        <label class="form-label">Informations additionnels du logement...</label>
                        <?= Html::activeTextarea($model, 'additionalInfo', ['class' => 'form-control', 'rows' => 5, 'cols' => 30]);?>
                    </div>
                </div>
            </div>
        </div>                    
        <div class="row clearfix">
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line">
                        <?php
                            //todo : if today, it will send "libre" via api
                        ?>
                        <?= Html::activeTextInput($model, 'disponibility', ['required'=>true, 'class' => 'datepicker form-control', 'id' => 'disponibility-date-picker', 'placeholder' => 'Please choose a date...', "data-dtp" => "dtp_Te5qz"]);?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="form-label">Active</label>
                    <input type="radio" name="House[active]" id="male" class="with-gap" value="1" <?=($model->active) ? "checked" : ""?>>
                    <label for="male">Oui</label>
                    <input type="radio" name="House[active]" id="female" value="0" class="with-gap" <?=(!$model->active) ? "checked" : ""?>>
                    <label for="female" class="m-l-20">Non</label>
                </div>
            </div>            
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line" style="border: none;"> 
                        <label class="form-label">Douche</label>
                        <?= Html::activeDropDownList($model, 'shower', ArrayHelper::map($showerTypes, 'key', 'value'), [
                            'prompt'=>'Type de douche', 
                            'data-selected'=>$model->shower,
                            'options' => [
                                'class' => 'rd-mailform-select'
                            ]
                        ]) ?>                        
                    </div>
                </div>
            </div>   
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line"> 
                        <label class="form-label"><?= \Yii::t('app', "Superficie")?></label>
                        <?= Html::activeTextInput($model, 'roomSize', ['required'=>true, 'class' => 'form-control']);?>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line">
                        <?= Html::activeTextInput($model, 'price', ['required'=>true, 'class' => 'form-control']);?>
                        <label class="form-label">Prix</label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-line" style="border: none;"> 
                        <label for="select-3"><?= \Yii::t('app', "DEVISE")?></label>
                        <?=Html::activeDropDownList($model, 'currency', ArrayHelper::map(Yii::$app->params['currencies'], 'key', 'value'), [
                            'options' => [
                                'data-selected'=>$model->currency,
                                'class' => 'rd-mailform-select'
                            ]
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="form-line" style="border: none;"> 
                        <label for="select-3"><?= \Yii::t('app', "MODE DE PAYEMENT")?></label>
                        <?= Html::activeDropDownList($model, 'paymentMode', ArrayHelper::map($paymentTypes, 'key', 'value'), [
                            'prompt'=>'Type de payement', 
                            'options' => [
                                'data-selected'=>$model->paymentMode,
                                'class' => 'rd-mailform-select'
                            ]
                        ]) ?>   
                    </div>
                </div>
            </div>            
        </div>     
        <div class="row clearfix">
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-line" style="border: none;">
                        <label for="select-3"><?= \Yii::t('app', "VILLE")?></label>
                        <?= Html::activeDropDownList($model, 'town', ArrayHelper::map($towns, 'key', 'value'), [
                            'prompt'=>'Toutes les villes', 
                            'id'=>'select-town',
                            'options' => [
                                'data-selected'=>$model->town,
                                'class' => 'rd-mailform-select'
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-line" style="border: none;">
                        <label for="select-4"><?= \Yii::t('app', "ZONE")?></label>
                        <?= Html::activeDropDownList($model, 'zone', ArrayHelper::map($zones, 'key', 'value'), [
                            'prompt'=>'Toutes les zones', 
                            'id'=>'select-zone',
                            'options' => [
                                'data-selected'=>$model->zone,
                                'class' => 'rd-mailform-select'
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>           
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-line" style="border: none;">
                        <label for="select-6"><?= \Yii::t('app', "RAYON")?></label>
                        <?= Html::activeDropDownList($model, 'radius', ArrayHelper::map($rayons, 'key', 'value'), [
                            'prompt'=>'Dans un rayon de', 
                            'id'=>'select-radius',
                            'options' => [
                                'data-selected'=>$model->radius,
                                'class' => 'rd-mailform-select'
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>            
        </div>          
        <div class="row clearfix">
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line">
                        <?= Html::activeTextInput($model, 'rentDeposit', ['required'=>true, 'class' => 'form-control']);?>
                        <label class="form-label">Caution Loyer</label>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <div class="form-line">
                        <?= Html::activeTextInput($model, 'electricityDeposit', ['required'=>true, 'class' => 'form-control']);?>
                        <label class="form-label">Caution Electricité</label>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="form-line" style="border: none;"> 
                        <label for="select-3"><?= \Yii::t('app', "TYPE DE COMPTEUR")?></label>
                        <?= Html::activeDropDownList($model, 'electricityCounterType', ArrayHelper::map($counterTypes, 'key', 'value'), [
                            'prompt'=>'Type de compteur', 
                            'id'=>'select-electricity-counter-type',
                            'options' => [
                                'data-selected'=>$model->electricityCounterType,
                                'class' => 'rd-mailform-select'
                            ]
                        ]) ?>   
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="form-group" style="padding-left: 20px">
                <?php if(!empty($model->pictures)):?>
                <div style="font-weight: bold;">Photos des maisons</div>
                    <?php foreach($model->pictures as $key=>$picture):?>                                             
                        <img src="<?= ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $model->referenceNumber;?>/<?= $picture;?>" class="picture-box"/>
                        <br/>
                        <a href="javascript:;" class="btn btn-sm btn-danger" style="border-radius: 15px;position: relative;margin-top: -65px;margin-left: 10px" title="Supprimer cette image"><b>X</b></a>
                    <?php endforeach;?>
                <?php endif;?>  
            </div>
        </div>
        <div class="form-group" style="padding-left: 6px">
            <input type="file" name="House[pictures][]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple/>
            <label for="file-7"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choisir les photos de la maison</strong></label>                        
        </div>    
        <button class="btn btn-primary waves-effect" type="submit">Ajouter</button>
    <?php ActiveForm::end(); ?>                          
</div> 
<script type="text/javascript">
    $("#form_validation input, #form_validation textarea").blur(function() {
        if($(this).val() != '') {           
            $(this).closest('div').addClass('focused');
        }
    });
</script>