<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Details du logement : #' . $house['referenceNumber'];
$agent_contact = (!empty($house['contact'])) ? $house['contact'] : [];
use common\components\ApiHelper;

if(!empty($house['rentDeposit']) && $house['rentDeposit'] != 0) {
    $nombre_payment_mode = ceil($house['rentDeposit'] / $house['price']);
}
?>
       <div class="container-fluid">
            <!--div class="block-header">
                <h2><?=$this->title;?></h2>
            </div-->
            <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?=$this->title;?>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?=Url::to(['houses/index']);?>" title="Voir tous les logements">Logements</a></li>
                                        <li><a href="<?=Url::to(['houses/update', 'id'=>$house['referenceNumber']]);?>" title="Modifier les informations">Modifier</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 220px">#</th>
                                        <th>Détails</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php if(count($house['pictures'])>1):?>Photos<?php else:?>Photo<?php endif;?></td>
                                        <td>
                                            <?php if(!empty($house['pictures'])):?>
                                                <?php foreach($house['pictures'] as $key=>$picture):?>                                             
                                                        <img src="<?= ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $house['referenceNumber'];?>/<?= !empty($house['pictures'][$key]) ? $house['pictures'][$key] : '';?>" style="width:175px; height:120px; border: 5px solid silver; border-radius: 5px;"/>
                                                <?php endforeach;?>
                                            <?php else :?>
                                                Aucune Image disponible
                                            <?php endif;?>                                                
                                        </td>
                                    </tr>                                      
                                    <tr>
                                        <td>Référence</td>
                                        <td><?=$house['referenceNumber'];?></td>
                                    </tr>                                     
                                    <tr>
                                        <td>Titre</td>
                                        <td><?=$house['titel'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Description</td>
                                        <td><?=$house['description'];?></td>
                                    </tr> 
                                    <tr>
                                        <td>Description Additionel</td>
                                        <td><?=$house['additionalInfo'];?></td>
                                    </tr>                                     
                                    <tr>
                                        <td>Type de Logement</td>
                                        <td><?= ucfirst($house['housingType']);?></td>
                                    </tr>   
                                     <tr>
                                        <td>Douche Interne</td>
                                        <td><?= ($house['shower'] == 'interne') ? 'Oui' : 'Non';?></td>
                                    </tr>                                    
                                    <tr>
                                        <td>Nombre de Pièces</td>
                                        <td><?= ucfirst($house['roomCount']);?></td>
                                    </tr>                                     
                                    <tr>
                                        <td>Addresse</td>
                                        <td><?=$house['zone'];?>, <?=$house['town'];?></td>
                                    </tr>                                     
                                    <tr>
                                        <td>Prix</td>
                                        <td><?=$house['price'];?> <?= Yii::$app->params['localeCurrency'];?>/<?=$house['paymentMode'];?></td>
                                    </tr>  
                                    <tr>
                                        <td>Avance</td>
                                        <td><?=$house['rentDeposit'];?> <?= Yii::$app->params['localeCurrency'];?> (<?= (isset($nombre_payment_mode)) ? $nombre_payment_mode . ' ' . $house['paymentMode'] : '';?>)</td>
                                    </tr>    
                                    <tr>
                                        <td>Avance Electricité</td>
                                        <td><?=$house['electricityDeposit'];?> <?= Yii::$app->params['localeCurrency'];?></td>
                                    </tr>   
                                    <tr>
                                        <td>Type de Compteur</td>
                                        <td><?= ucfirst($house['electricityCounterType']);?></td>
                                    </tr> 
                                    <tr>
                                        <td>Agent</td>
                                        <td>
                                            <?php if(!empty($agent_contact)) : ?> 
                                            <div>
                                                Nom Complet : <?=$agent_contact['firstName'];?> <?=$agent_contact['surName'];?>                                                                                       
                                            </div>
                                            <div>
                                                Téléphone : <?=$agent_contact['telefonNr'];?>
                                            </div>
                                            <div>
                                                Email : <?=$agent_contact['email'];?>
                                            </div>
                                            <?php else:?>
                                                ---
                                            <?php endif;?>     
                                        </td>
                                    </tr>                                     
                                    <tr>
                                        <td>Ajouté le :</td>
                                        <td><?= ucfirst($house['publicationDate']);?></td>
                                    </tr>                                     
                                </tbody>
                            </table>
                            <a class="btn btn-primary" href="<?=Url::to(['houses/index']);?>"><< Retour</a>                                                
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Table -->         
        </div>