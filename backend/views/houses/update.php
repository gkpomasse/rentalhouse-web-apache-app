<?php
    use yii\helpers\Url;
    use common\models\House;
    
    $this->title = 'Modifier les informations du logement <b>#' . $model->referenceNumber . '</b>';
    //fix disponibility
    if($model->disponibility == House::HOUSE_AVAILABLE) {
        $model->disponibility = date('d/m/Y');
    }
?>
<div class="container-fluid">
    <!--div class="block-header">
        <h2><?=$this->title;?></h2>
    </div-->
    <!-- Basic Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?=$this->title;?>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?=Url::to(['houses/index']);?>" title="Liste de tous les logements">Liste</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                    <?=Yii::$app->controller->renderPartial('_form', [
                        'data' => $data,
                        'model' => $model,
                        'active' => true
                    ]);?>
                </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
</div>
<script type="text/javascript">
    //auto-select housing type
    $('#select-housing-type option').filter(function () { 
        return $(this).text() == "<?=$model->housingType;?>"; 
    }).prop('selected', true);
    //auto-select town
    $('#select-town option').filter(function () { 
        return $(this).text() == "<?=$model->town;?>"; 
    }).prop('selected', true);
    //auto-select housing type
    $('#select-zone option').filter(function () { 
        return $(this).text() == "<?=$model->zone;?>"; 
    }).prop('selected', true);
    //auto-select radius
    $('#select-radius option').filter(function () { 
        return $(this).text() == "<?=$model->radius;?>"; 
    }).prop('selected', true);    
</script>   