<?php
    use yii\helpers\Url;
    $this->title = 'Ajouter un nouveau logement';
    $model->disponibility = date('d/m/Y');
?>
<div class="container-fluid">
    <!--div class="block-header">
        <h2><?=$this->title;?></h2>
    </div-->
    <!-- Basic Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?=$this->title;?>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?=Url::to(['houses/index']);?>" title="Liste de tous les logements">Liste</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                    <?=Yii::$app->controller->renderPartial('_form', [
                        'data' => $data,
                        'model' => $model,
                        'active' => false
                    ]);?>
                </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
</div>
