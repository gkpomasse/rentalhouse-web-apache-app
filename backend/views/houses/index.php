<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\ApiHelper;

$this->title = 'Logements disponibles';
/*echo '<pre>';
print_r($results);
echo '<pre>';
exit();*/
?>
<div class="container-fluid">
    <!--div class="block-header">
        <h2><?=$this->title;?></h2>
    </div-->
    <!-- Basic Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?=$this->title;?>
                        <small>Tous les logements sont listés dans cette section</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?=Url::to(['houses/create']);?>" title="Ajouter un nouveau logement">Nouveau</a></li>
                                <li><a href="javascript:void(0);" id="list-active">Active</a></li>
                                <li><a href="javascript:void(0);" id="list-unactive">Non Active</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body table-responsive">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#home" id="tab-home" data-toggle="tab">TOUT (<?=count($allHouses)?>)</a></li>
                        <li role="presentation"><a href="#available" id="tab-available" data-toggle="tab">DISPONIBLE (<?=count($activeProperties)?>)</a></li>
                        <li role="presentation"><a href="#awaiting" id="tab-awaiting" data-toggle="tab">EN ATTENTE (<?=count($notActiveProperties)?>)</a></li>
                        <li role="presentation"><a href="#archive" id="tab-archive" data-toggle="tab">ARCHIVE (<?=count($deletedProperties)?>)</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                            <b>Liste de tous les logements</b>
                            <?=Yii::$app->controller->renderPartial('_listhouse', [
                                'houses' => $allHouses
                            ]);?>                                     
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="available">
                            <b>Liste des logements disponibles</b>
                            <?=Yii::$app->controller->renderPartial('_listhouse', [
                                'houses' => $activeProperties
                            ]);?>                                 
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="awaiting">
                            <b>Liste des logements en attente</b>
                            <?=Yii::$app->controller->renderPartial('_listhouse', [
                                'houses' => $notActiveProperties
                            ]);?>                                    
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="archive">
                            <b>Liste des logements archivés</b>
                            <?=Yii::$app->controller->renderPartial('_listhouse', [
                                'houses' => $deletedProperties,
                                'deleted' => true
                            ]);?>                                    
                        </div>
                    </div>                            
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
</div>