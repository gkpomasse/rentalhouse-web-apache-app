<?php

namespace backend\controllers;
use common\components\ApiHelper;

class CategoryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $categories = ApiHelper::loadSingleQuestions(ApiHelper::SEARCH_PANEL_HOUSETYPE);
        return $this->render('index', [
            'models' => $categories
        ]);
    }
}