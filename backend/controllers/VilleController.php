<?php

namespace backend\controllers;
use common\components\ApiHelper;

class VilleController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $models = ApiHelper::loadSingleQuestions(ApiHelper::SEARCH_PANEL_TOWN);
        return $this->render('index', [
            'models' => $models
        ]);
    }

}
