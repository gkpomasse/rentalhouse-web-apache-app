<?php

namespace backend\controllers;

use Yii;
use linslin\yii2\curl;
use common\components\ApiHelper;
use yii\filters\AccessControl;
use common\models\House;
use yii\web\UploadedFile;
use common\components\UserApiLoggedFilter;

class HousesController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => UserApiLoggedFilter::className(),
                'only' => ['index', 'ajax-load-sorting-result', 'search', 'category', 'view', 'create', 'update'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'ajax-load-sorting-result', 'search', 'category', 'view', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'ajax-load-sorting-result', 'search', 'category', 'view', 'create', 'update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * @return string
     */
    public function actionIndex($sortBy = '')
    {
        $sortingResults = ApiHelper::getHomeResultsFilterOptions();
        $baseParams = ApiHelper::defaultAllPropertiesParams();

        //get all properties
         /*   if(!empty($sortBy) && !empty($sortingResults[$sortBy])) {               
                $baseParams['sortBy'] = $sortBy;
                $allHouses =  ApiHelper::getAllProperties($baseParams);
            } else {
                //todo : custom later while integrating pagination
                $allHouses =  ApiHelper::getAllProperties();
            }*/
            
        //get active properties
            $baseParams['active'] = true;
            $activeProperties = ApiHelper::getAllProperties($baseParams, false);
            
        //get not active properties
            $baseParams['active'] = false;
            $notActiveProperties = ApiHelper::getAllProperties($baseParams, false);
            
        //get not active properties
            unset($baseParams['active']);
            $baseParams['deleted'] = true;
            $deletedProperties = ApiHelper::getAllProperties($baseParams);   
            
        //get archived properties
            $archivedProperties = ApiHelper::getArchiveProperties();
            //var_dump($archivedProperties);
            //exit();
            $allHouses = array_merge($activeProperties, $notActiveProperties);            
            
        return $this->render('index', [
            'allHouses' => $allHouses,
            'activeProperties' => $activeProperties,
            'notActiveProperties' => $notActiveProperties,
            'deletedProperties' => $archivedProperties
        ]);
    }

    /**
     * @param type $sortBy
     * @return type
     */
    public function actionAjaxLoadSortingResult($sortBy='')
    {
        $baseParams = ApiHelper::defaultAllPropertiesParams();
        $baseParams['sortBy'] = $sortBy;
        $results =  ApiHelper::getAllProperties($baseParams);
        $output = $this->renderPartial('_results_list', [
            'results' => $results
        ]);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'status' => 'success',
            'data' => $output
        ];
    }

    /**
     * search properties which match requests from user by calling related api
     * @return string
     */
    public function actionSearch($viewType = 'listview')
    {
        $post = $_POST;
        //
        $housingType = $post['search_panel_housingtype'];
        $roomCount = $post['search_panel_room_count'];
        $town = $post['search_panel_town'];
        $zone = $post['search_panel_zone'];
        $price_min = $post['search_panel_price_min'];
        $price_max = $post['search_panel_price_max'];
        $area_min = $post['search_panel_surface_min'];
        $area_max = $post['search_panel_surface_max'];
        $keywords = $post['search_panel_keyword'];
        $publication_date = $post['search_panel_publication_date'];
        $radius = $post['search_panel_radius'];

        //parameters to be sent to api
        $params = [
            'housingType' => $housingType,
            'roomCount' => $roomCount,
            'town' => $town,
            'zone' => $zone,
            'radius' => $radius,
            'priceMin' => (int) $price_min,
            'priceMax' => (int) $price_max,
            'roomSizeMin' => (int) $area_min,
            'roomSizeMax' => (int) $area_max,
            'keyword' => $keywords,
            'publicationDate' => $publication_date
        ];
		//echo '<pre>';
       // print_r($params);
		//echo '</pre>';
        //call api with these paramaters
        $curl = new curl\Curl();
        $searchQuery = json_encode($params);
        //
        $results = [];
        //var_dump($searchQuery);
        $response = $curl->setRequestBody($searchQuery)
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($searchQuery)
            ])
            ->post(ApiHelper::API_URL . ApiHelper::SEARCH_HOUSE_URL);

        $data = json_decode($response, true);
        //print_r($data);exit();
        if(!empty($data['_embedded']))
            $results = $data['_embedded']['houses'];

        //get search data
        $searchData = ApiHelper::buildSearchData();

        $viewType = (in_array($viewType, ApiHelper::resultDisplayMode())) ? $viewType : ApiHelper::LIST_VIEW_MODE;

        return $this->render('search', [
            'results' => $results,
            'searchData' => $searchData,
            'searchedParams' => $params,
            'viewType' => $viewType
        ]);
    }

    /**
     * @return string
     */
    public function actionCategory($id, $viewType = 'listview')
    {
        /**
         * Retrive properties
         */
        $curl = new curl\Curl();
        $params = ApiHelper::getCategoryPropertyParams();
        $params['housingType'] = $id;
        $encodedParams = json_encode($params);
        $response = $curl->setRequestBody($encodedParams)
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen($encodedParams)
            ])
            ->post(ApiHelper::API_URL . ApiHelper::LIST_HOUSE_WITH_PAGINATION_URL);

        $data = json_decode($response, true);
        //var_dump($encodedParams);exit();
        if(!empty($data['_embedded']))
            $results = $data['_embedded']['houses'];
        else
            $results = [];

        //var_dump($results);

        //get search data
        $searchData = ApiHelper::buildSearchData();

        $viewType = (in_array($viewType, ApiHelper::resultDisplayMode())) ? $viewType : ApiHelper::LIST_VIEW_MODE;

        //var_dump($houses);exit();
        return $this->render('category', [
            'results' => $results,
            'searchData' => $searchData,
            'searchedParams' => $params,
            'id' => $id,
            'viewType' => $viewType
        ]);
    }

    /**
     * view details about one property
     * @return string
     */
    public function actionView($id)
    {
        $loadedProperty = ApiHelper::viewProperty($id);
        if(!empty($loadedProperty)) {
            return $this->render('view', [
                'house' => $loadedProperty
            ]);            
        } else {
            throw new Exception("Ce Logement n'existe pas!!!");
        }
    }

    /**
     * submit new property to us
     * @return string
     */
    public function actionCreate() {
        $data = [];
        
        //model
            $model = new House;
        
        //get search data
            $data['questions'] = ApiHelper::buildSearchData(); 
            
        //get dropdowns options
            $data['options'] = [
                'payment_modes' => House::payment_modes,
                'electricity_count_types' => House::electricity_counter_types,
                'shower_types' => House::shower_types
            ];
            
        //
            if($model->load(Yii::$app->request->post())) {
                $model->pictures = UploadedFile::getInstances($model, 'pictures');
                if($model->validate()) {
                    
                } else {
                    var_dump($model->errors);
                }
                
                exit();
            }
            
        return $this->render('create', [
            'data' => $data,
            'model' => $model
        ]);
    }
    
    /**
     * update property data
     */
    public function actionUpdate($id) {
        $loadedProperty = ApiHelper::viewProperty($id);
        if(!empty($loadedProperty)) {
            //var_dump($loadedProperty);exit();
            //model
            $model = new House;
            $model->attributes = $loadedProperty;
            
        //get search data
            $data['questions'] = ApiHelper::buildSearchData(); 
            
        //get dropdowns options
            $data['options'] = [
                'payment_modes' => House::payment_modes,
                'electricity_count_types' => House::electricity_counter_types,
                'shower_types' => House::shower_types
            ];
            
            return $this->render('update', [
                'model' => $model,
                'data' => $data
            ]); 
        } else {
            throw new Exception("Ce Logement n'existe pas!!!");
        }
    }

    public function actionDelete($id) {
        $response = ApiHelper::deleteHouse($id);
        if($response["content"]["House deleted: "] === true) {//house properly deleted
            $output = [
                'status' => 'success',
                'message' => 'Le logement ' . $id  . ' a été correctement supprimé.'
            ];
        } else {
            $output = [
                'status' => 'error',
                'message' => 'Un problème est survenu lors de la suppression du logement #' . $id . '.'
            ];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $output;
    }
}
