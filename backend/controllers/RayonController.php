<?php

namespace backend\controllers;
use common\components\ApiHelper;

class RayonController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $models = ApiHelper::loadSingleQuestions(ApiHelper::SEARCH_PANEL_RAYON);
        return $this->render('index', [
            'models' => $models
        ]);
    }

}
