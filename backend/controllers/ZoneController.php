<?php

namespace backend\controllers;
use common\components\ApiHelper;

class ZoneController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $models = ApiHelper::loadSingleQuestions(ApiHelper::SEARCH_PANEL_ZONE);
        return $this->render('index', [
            'models' => $models
        ]);
    }

}
