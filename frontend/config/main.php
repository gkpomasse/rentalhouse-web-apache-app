<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

use \yii\web\Request;
$baseUrl = str_replace('/frontend/www', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'fr',
    'name' => 'Bénin Immobilier',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'fr',
                    'basePath' => '@app/messages'
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'fr',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ], 
                ],    
            ],
        ],        
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            //'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'acceuil' => 'site/index',
                'category-logement' => 'houses/category',
                'voir-logement' => 'houses/view',
                'nos-logements' => 'houses/index',
                'nous-contacter' => 'site/contact',
                'connexion' => 'site/login',
                'inscription' => 'site/registration',
                'mon-compte' => 'site/account'
            ],
        ],
        'redis' => [
           'class'    => 'yii\redis\Connection',
           'hostname' => 'localhost',
           'port'     => 6379,
           'database' => 0,
		   'password' => '8lqwfywoh805xqc'
        ],
        'session' => [ 
           'class' => 'yii\redis\Session', 
        ],
        'cache' => [
           'class' => 'yii\redis\Cache',
        ],		
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'srv22.dsbsrv.de',
                'username' => 'aide@beninimmobilier.com',
                'password' => 'dodniUKeesVob#',
                'port' => '465',
                'encryption' => 'ssl',
            ]            
        ],          
    ],
    'params' => $params,
];