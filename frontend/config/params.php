<?php

return [
    'adminEmail' => 'admin@example.com',
    'contactEmail' => 'aide@beninimmobilier.com',
    //'contactEmail' => 'gkpomasse@gmail.com',
    'contactPhone' => '0022997620455',
    'contactPhone2' => '(229) 97 62 04 55',
    'contactPhone3' => '0022961682553',
    'country' => 'Benin',
    'facebookUrl' => 'https://www.facebook.com/Benin-Immobilier-593293997722664/',
    'twitterUrl' => 'https://twitter.com/BeninImmobilier',
    'allowedUserRole' => 'ROLE_USER'//permet de se connecter en tant qu'utilisateur
];
