window.global = window.global || {};

window.global.addParam = function (url, key, value) {
    return url + ( url.match( /[\?]/g ) ? '&' : '?' ) + key + '=' + value;
};

window.global.removeParam = function(url, key) {
    var rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

window.global.getParam = function (sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

window.houses = window.houses || {};
window.houses.getHousesBySort = function (sortingValue) {
    var dfd = $.Deferred();
    var response = {};
    //append view type
    var selected_view_type = $('.sorting a.active').attr("data-view-type");
    var category = window.global.getParam('id');
    if(typeof category === "undefined") {
        category = "";
    }
    $.ajax({
        url: jsBaseUrl + 'houses/ajax-load-sorting-result?sortBy=' + sortingValue + '&viewType=' + selected_view_type + '&category=' + category,
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: "json",
        cache: true,
        timeout: 6000000,
        beforeSend: function () {
            $('#filter-dropdown-loading').show();
        },
        success: function (msg) {
            response = msg;
            $('#filter-dropdown-loading').hide();
            $('#houses-results').html(msg.data);
        },
        complete: function (jqXHR, textStatus) {
            dfd.resolve(response);
        },
        failure: function (transport) {
            dfd.reject({status: 'fail'});
        },
        exception: function (transport) {
            dfd.reject({status: 'fail'});
        }
    });
    return dfd.promise();
};
//
window.houses.load = function (url) {
    var dfd = $.Deferred();
    var response = {};
    $.ajax({
        url: url,
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: "json",
        cache: true,
        timeout: 6000000,
        beforeSend: function () {
            $('#filter-dropdown-loading').show();
        },
        success: function (msg) {
            response = msg;
            $('#filter-dropdown-loading').hide();
            $('#houses-results').html(msg.data);
        },
        complete: function (jqXHR, textStatus) {
            dfd.resolve(response);
        },
        failure: function (transport) {
            dfd.reject({status: 'fail'});
        },
        exception: function (transport) {
            dfd.reject({status: 'fail'});
        }
    });
    return dfd.promise();
};
//
$('#contact-form').submit(function(e) {
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $('#contact-form').serialize(),
        beforeSend: function () {
            
        },
        success: function (msg) {
            //console.log(msg);
            if(msg.type == 'danger') {
                $('#captcha-validation span.error').html('Vous devez confirmer que vous n\'êtes pas un robot.');
            } else if(msg.type == 'success') {
                new PNotify({
                    title: 'Notification',
                    text: 'Merci, nous avons reçu avec succès votre requête.',
                    type: msg.type,
                    text_escape: true
                });
                //reload page
                location.reload();
            }
        }
    });    
    e.preventDefault();
});