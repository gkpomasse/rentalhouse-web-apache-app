<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/style.css',
        'plugins/pnotify/pnotify.custom.min.css'
    ];
    public $js = [     
        'js/core.min.js',
        'js/script.js',        
        'js/ajax-request.js',
        'plugins/pnotify/pnotify.custom.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset', current template not supporting bootstrap well
    ];
}
