<?php
/**
 * Created by Ghislain KPOMASSE
 * Date: 10/04/2018
 * Time: 00:51
 */

namespace app\components;


class Utils
{
    /**
     * permet d'obtenir le pluriel des mots basés sur la quantité
     * @param int $count
     * @param $word
     * @return string
     */
    public static function pluralWords($count=1, $word) {
        return ($count > 1) ? $count . ' ' . $word . 's' : $count . ' ' . $word;
    }

    /**
     * Convertit une date ou un timestamp en français
     */
    public static function dateToFrench($date, $format) 
    {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
    }
}