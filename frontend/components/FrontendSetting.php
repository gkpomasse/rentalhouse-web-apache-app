<?php

namespace app\components;

/**
 * FrontendSetting contains all default settings for manage easily frontend
 *
 * @author Ghislain KPOMASSE <coder@ghislainkpomasse.com>
 */
class FrontendSetting {
    /**
     * @var integer slider max show
     */
    const HOMEPAGE_SLIDER_MAX = 4;
    /**
     * @var integer homepage recent limit
     */
    const RECENT_PROPERTY_LIMIT = 6;
    /**
     * @var integer property limit per page
     */
    const LIST_PROPERTY_PER_PAGE = 10;
    /**
     * @var integer max category per page
     */
    const HOMEPAGE_CATEGORY_PER_ROW_MAX = 2;
    /**
     * @var integer search form default publication dropdown value
     */
    const SEARCH_DEFAULT_PUBLICATION_DATE = "THIS_YEAR";
    /**
     * @var integer
     */
    const HOUSES_DEFAULT_PAGE_NUMBER = 0;
    /**
     * @var string
     */
    const CONTACT_PAGE_SUBJECT_TEXT = "Requête provenant de la page Contact";
}
