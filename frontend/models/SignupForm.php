<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\components\ApiHelper;

/**
 * Signup form
 */
class SignupForm extends Model
{
    //public $username; (note: username has been moved to telephone then all validations can be let isnide api only)
    public $email;
    public $password;
    public $confirmationPassword;
    public $telephoneNr;
    public $firstName;
    public $surName;
    public $roles;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //all required attributes
            [['telephoneNr', 'firstName', 'surName', 'password', 'confirmationPassword'], 'required'],

            //numero de téléphone validation
            ['telephoneNr', 'integer', "message" => "Votre numéro de téléphone doit être composer de nombre uniquement."],
            
            //email validation
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            
            //mot de passe validation
            ['password', 'string', 'min' => 6, 'tooShort' => 'Votre mot de passe doit compter au minimum 6 caractères.'],
            ['confirmationPassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Erreur de confirmation du mot de passe." ]
        ];
    }
    
    /**
     * translated label names
     * @return array
     */
    public function attributeLabels() {
        return [
            'telephoneNr' => "Numéro de téléphone",
            'email' => 'Adresse Email',
            'surName' => 'Nom de Famille',
            'firstName' => 'Prénom',
            'password' => "Mot de Passe",
            'confirmationPassword' => "Confirmation",
            'rememberMe' => "Se souvenir de Moi"
        ];         
    }
    

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        //return value
            //$_return = false;
        
        $params = [
            "firstName" => $this->firstName,
            "surName" => $this->surName,
            "password" => $this->password,
            "telephoneNr" => $this->telephoneNr,
            "email" => $this->email,
            "roles" => [
                Yii::$app->params['allowedUserRole']
            ]
        ];
        
        $authenticated = ApiHelper::signup($params);
        //
        return (!$authenticated) ? $this->addError("telephoneNr", "Cet Utilisateur existe déjà") : true;
    }
    
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findWithApi($this->username, $this->password);
        }

        return $this->_user;
    }    
}
