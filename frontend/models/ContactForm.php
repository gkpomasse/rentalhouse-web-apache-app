<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $nom;
    public $prenom;
    public $email;
    public $subject;
    public $body;
    //public $verifyCode;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['nom', 'prenom', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        $fullname = $this->nom . ' ' . $this->prenom;
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $fullname])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
    
    /*public function beforeValidate() {
        parent::beforeValidate();
        $this->subject = "Requête provenant de la page Contact";
    }*/
}
