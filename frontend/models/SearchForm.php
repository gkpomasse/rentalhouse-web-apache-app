<?php
/**
 * @author Ghislain KPOMASSE <coder@ghislainkpomasse.com>
 */

namespace frontend\models;

use Yii;
use common\models\House;

/**
 * Description of SearchForm
 *
 * @author gkcoder
 */
class SearchForm extends House {
    public $keyword;
    public $publicationDate;
    public $minPrice;
    public $maxPrice;
    public $minSurface;
    public $maxSurface;
    
    public function rules()
    {
        return [
            // email has to be a valid email address
            ['titel', 'string', 'min' => 5, 'max' => 120],
            ['currency', 'string'],
            ['price', 'double'],
            [['pictures'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 4],
            [['titel', 'referenceNumber', 'description', 'shower', 'price', 'currency', 'paymentMode', 'housingType', 'roomCount', 'roomSize', 'town', 'zone', 'additionalInfo', 'rentDeposit', 'electricityDeposit', 'electricityCounterType', 'disponibility', 'active', 'userName', 'keyword', 'publicationDate', 'minPrice', 'maxPrice', 'minSurface', 'maxSurface'], 'safe']
        ];
    }    
}
