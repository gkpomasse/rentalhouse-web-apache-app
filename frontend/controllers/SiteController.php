<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use linslin\yii2\curl;
use common\components\ApiHelper;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\SearchForm;
use app\components\FrontendSetting;
use common\components\UserApiLoggedFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => UserApiLoggedFilter::className(),
                'only' => ['index', 'contact'],
            ],            
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'registration'],
                'rules' => [
                    [
                        'actions' => ['registration'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //get search data
            $searchData = ApiHelper::buildSearchData();

        //get recent properties
            $recentProperties =  ApiHelper::getLatestProperties();

        //get housing types count
            $housingTypeCounts =  ApiHelper::getHousingTypeCount();

        //display houses configuration
            $displayConfigurations = $this->propertyDisplayConfiguration();
            
        //model represent search form
            $searchModel = new SearchForm;
            $searchModel->publicationDate = FrontendSetting::SEARCH_DEFAULT_PUBLICATION_DATE;

        return $this->render('index', [
            'data' => $searchData,
            'houses' => $recentProperties,
            'displayConfigurations' => $displayConfigurations,
            'housingTypeCounts' => $housingTypeCounts,
            'searchModel' => $searchModel
        ]);
    }
    /**
     * 
     */
    public function actionTestEmail() {
        Yii::$app->mailer->compose()
                   ->setTo("gkpomasse@gmail.com")
                   ->setFrom("aide@beninimmobilier.com")
                   ->setSubject("Test")
                   ->setTextBody("Test contact")
                   ->send();       
    }

    /**
     * @return array
     */
    public function propertyDisplayConfiguration() {
        return [
            [
                "delay" => ".2s",
            ],
            [
                "delay" => ".4s",
            ],
            [
                "delay" => ".6s",
            ],
            [
                "delay" => ".2s",
            ],
            [
                "delay" => ".4s",
            ],
            [
                "delay" => ".6s",
            ],
            [
                "delay" => ".4s",
            ],
        ];
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->setFlash('success', 'Bienvenue ' . Yii::$app->user->identity->surName . ' ' . Yii::$app->user->identity->firstName . ',<br/> Vous êtes maintenant connecté.');
            return $this->goHome();                  
        } else {
            if(Yii::$app->request->isPost)
            {//means that something wrong
                Yii::$app->session->setFlash('error', $model->getFirstError("password"));
            }
            $model->password = '';//reset password

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        //logout message
        Yii::$app->session->setFlash('notice', 'Vous êtes maintenant déconnecté.');

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();        
        if ($model->load(Yii::$app->request->post())) {
            $model->subject = FrontendSetting::CONTACT_PAGE_SUBJECT_TEXT;
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->validate()) {//success form validation                
                if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
                    //your site secret key
                    $secret = '6LdC7EsUAAAAABJYpuTwVNKGtWPTTlympzlxHFOM';
                    //get verify response data
                    $c = curl_init('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                    $verifyResponse = curl_exec($c);
                    $responseData = json_decode($verifyResponse);
                    if(!empty($responseData) && $responseData->success) {
                        try
                        {
                            $model->sendEmail(Yii::$app->params['contactEmail']);
                            //send success mail
                                /*Yii::$app->mailer->compose()
                                    ->setTo($model->email)
                                    ->setFrom([Yii::$app->params['contactEmail'] => Yii::$app->name])
                                    ->setSubject("Requête reçu avec succès.")
                                    ->setTextBody("Salut, " . $model->nom . ' ' . $model->prenom . "<br/> Votre requête est en cours de traitement. Merci de patienter!")
                                    ->send(); */
                            //
                            $responseArray = [
                                'type' => 'success',
                                'message' => "Everything went well!"
                            ];
                        } catch (\Exception $e) {
                            $responseArray = [
                                'type' => 'danger',
                                'message' => $e->getMessage()
                            ];
                        }

                        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                return $responseArray;
                        } else {
                                echo $responseArray['message'];
                        }
                    } else {
                        $errorMessage = 'Robot verification failed, please try again.';
                        return [
                                'type' => 'danger',
                                'message' => $errorMessage
                        ];
                    }				
                } else {
                    $errorMessage = 'Please click on the reCAPTCHA box.';
                    return [
                            'type' => 'danger',
                            'message' => $errorMessage
                    ];				
                }
            } else {//return error message
                $errorMessage = 'Un Problème est survenu durant la soumission du formulaire!';
                return [
                        'type' => 'danger',
                        'message' => $errorMessage
                ];				
            }                
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);                
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionRegistration()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->signup()) {
                $login = new LoginForm();
                $login->username = $model->telephoneNr;
                $login->password = $model->password;
                if ($login->login()) {
                    Yii::$app->session->setFlash('success', 'Bienvenue ' . Yii::$app->user->identity->surName . ' ' . Yii::$app->user->identity->firstName . ',<br/> Vous êtes maintenant connecté.');
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
