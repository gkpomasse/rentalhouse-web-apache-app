<?php

namespace frontend\controllers;

use Yii;
use linslin\yii2\curl;
use common\components\ApiHelper;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\FrontendSetting;

class HousesController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'ajax-load-sorting-result', 'search', 'category', 'view', 'create'],
                'rules' => [
                    [
                        'actions' => ['index', 'ajax-load-sorting-result', 'search', 'category', 'view'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'search' => ['post']
                ],
            ],
        ];
    }
    
    /**
     * @return string
     */
    public function actionIndex($sortBy = '', $viewType = 'listview', $page = 0)
    {
        $searchModel = new \frontend\models\SearchForm;
        $searchModel->publicationDate = FrontendSetting::SEARCH_DEFAULT_PUBLICATION_DATE;
        
        $sortingResults = ApiHelper::getHomeResultsFilterOptions();
        
        //build parameters
        $baseParams = ApiHelper::defaultAllPropertiesParams();
        $baseParams['pageNr'] = $page;

        //get all properties
            if(!empty($sortBy) && !empty($sortingResults[$sortBy])) {               
                $baseParams['sortBy'] = $sortBy;                              
            }
            
        //get results
            $results =  ApiHelper::getAllProperties($baseParams);
            
        //get result count 
        $housesCount = (!empty($results[0])) ? $results[0]['resultCount'] : 0;
        //$housesCount = 4;
        //var_dump($results); exit();

        //get search data
        $searchData = ApiHelper::buildSearchData();

        $viewType = (in_array($viewType, ApiHelper::resultDisplayMode())) ? $viewType : ApiHelper::LIST_VIEW_MODE;
        
        if (Yii::$app->request->isAjax) {
            $output = $this->renderPartial('_results_list_' . $viewType, [
                'results' => $results,
                'housesCount' => $housesCount,
                'pageNumber' => $page,
                'viewType' => $viewType
            ]);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'data' => $output
            ];            
        } else {
            return $this->render('index', [
                'results' => $results,
                'searchData' => $searchData,
                'searchedParams' => [],
                'viewType' => $viewType,
                'searchModel' => $searchModel,
                'housesCount' => $housesCount,
                'pageNumber' => $page
            ]);
        }
    }

    /**
     * 
     * @param type $sortBy
     * @param type $viewType
     * @return type
     */
    public function actionAjaxLoadSortingResult($category = '', $sortBy = '', $viewType = 'listview', $page=0)
    {
        $baseParams = ApiHelper::defaultAllPropertiesParams();
        $baseParams['sortBy'] = $sortBy;
        $baseParams['pageNr'] = $page;
        if(!empty($category)) {
            $baseParams['housingType'] = $category;
        }
        $results =  ApiHelper::getAllProperties($baseParams);
        //get result count 
        $housesCount = (!empty($results[0])) ? $results[0]['resultCount'] : 0;
        //$housesCount = 4;
        $output = $this->renderPartial('_results_list_' . $viewType, [
            'results' => $results,
            'housesCount' => $housesCount,
            'pageNumber' => $page,
            'viewType' => $viewType
        ]);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'status' => 'success',
            'data' => $output
        ];
    }

    /**
     * search properties which match requests from user by calling related api
     * @return string
     */
    public function actionSearch($viewType = 'listview')
    {
        $searchModel = new \frontend\models\SearchForm;
        if($searchModel->load(Yii::$app->request->post())) {
            //parameters to be sent to api
            $params = [
                'housingType' => $searchModel->housingType,
                'roomCount' => $searchModel->roomCount,
                'town' => $searchModel->town,
                'zone' => $searchModel->zone,
                'radius' => $searchModel->radius,
                'priceMin' => (int) $searchModel->minPrice,
                'priceMax' => (int) $searchModel->maxPrice,
                'roomSizeMin' => (int) $searchModel->minSurface,
                'roomSizeMax' => (int) $searchModel->maxSurface,
                'keyword' => $searchModel->keyword,
                'publicationDate' => $searchModel->publicationDate
            ];
            //echo json_encode($params);
            //exit();
            $results = ApiHelper::searchHouses($params);
            
            //get result count 
            $housesCount = (!empty($results[0])) ? $results[0]['resultCount'] : 0;
            //$housesCount = 4;            

            //get search data
            $searchData = ApiHelper::buildSearchData();

            $viewType = (in_array($viewType, ApiHelper::resultDisplayMode())) ? $viewType : ApiHelper::LIST_VIEW_MODE;

            return $this->render('search', [
                'results' => $results,
                'searchData' => $searchData,
                'searchedParams' => $params,
                'viewType' => $viewType,
                'searchModel' => $searchModel,
                'housesCount' => $housesCount,
                'pageNumber' => \app\components\FrontendSetting::HOUSES_DEFAULT_PAGE_NUMBER
            ]);            
        }
    }

    /**
     * @return string
     */
    public function actionCategory($id, $viewType = 'listview', $page = 0)
    {
        $searchModel = new \frontend\models\SearchForm;
        /**
         * Retrive properties
         */
        $params = ApiHelper::getCategoryPropertyParams();
        
        //edit base params
        $params['housingType'] = $id;
        $params['pageNr'] = $page;
        //var_dump($params);
        //exit();
        
        $results =  ApiHelper::getAllProperties($params);

        $viewType = (in_array($viewType, ApiHelper::resultDisplayMode())) ? $viewType : ApiHelper::LIST_VIEW_MODE;
        
        $housingTypeCount = ApiHelper::getHousingTypeCount();
        
        //get result count 
        $housesCount = (!empty($housingTypeCount[$id])) ? $housingTypeCount[$id] : 0;
        //var_dump($housesCount);exit();

        //var_dump($houses);exit();
        if (Yii::$app->request->isAjax) {
            $output = $this->renderPartial('_results_list_' . $viewType, [
                'results' => $results,
                'viewType' => $viewType,
                'housesCount' => $housesCount,
                'pageNumber' => $page,
                'customParams' => ['id'=>$id]
            ]);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'data' => $output
            ];            
        } else {   
            //get search data
            $searchData = ApiHelper::buildSearchData();
            return $this->render('category', [
                'results' => $results,
                'searchData' => $searchData,
                'searchedParams' => $params,
                'id' => $id,
                'viewType' => $viewType,
                'searchModel' => $searchModel,
                'housesCount' => $housesCount,
                'pageNumber' => \app\components\FrontendSetting::HOUSES_DEFAULT_PAGE_NUMBER            
            ]);
        }
    }

    /**
     * view details about one property
     * @return string
     */
    public function actionView($id)
    {
        $loadedProperty = ApiHelper::viewProperty($id);
        
        //var_dump($loadedProperty);exit();
        
        if(empty($loadedProperty) || !empty($loadedProperty['error'])) {
            return $this->redirect('site/error');
            exit();
        }        
        
        //display houses configuration
            $displayConfigurations = $this->propertyDisplayConfiguration();
        
        //get recent properties
            $recentProperties =  ApiHelper::getProperties([
                                    'pageNr' => 0,
                                    'pageSize' => 3,
                                    'sortBy' => 'SORT_BY_DATE'
                                ]);
            
        $location = \common\components\Map::decodeAddress($loadedProperty['zone'] . ',' . $loadedProperty['town'] . ', ' . Yii::$app->params['country']);

        //var_dump($loadedProperty);exit();
        return $this->render('view', [
            'data' => $loadedProperty,
            'houses' => $recentProperties,
            'displayConfigurations' => $displayConfigurations,
            'location' => $location
        ]);
    }
    
    /**
     * @return array
     */
    public function propertyDisplayConfiguration() {
        return [
            [
                "delay" => ".2s",
            ],
            [
                "delay" => ".4s",
            ],
            [
                "delay" => ".6s",
            ],
            [
                "delay" => ".2s",
            ],
            [
                "delay" => ".4s",
            ],
            [
                "delay" => ".6s",
            ],
            [
                "delay" => ".4s",
            ],
        ];
    }    

    /**
     * submit new property to us
     * @return array
     */
    public  function actionCreate() {
        return $this->render('create', [
        ]);
    }
}
