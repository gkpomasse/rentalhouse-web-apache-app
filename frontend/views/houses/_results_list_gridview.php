<?php
    use yii\helpers\Url;
    use common\components\ApiHelper;
    use app\components\FrontendSetting;
    //
    $pages = ceil($housesCount / FrontendSetting::LIST_PROPERTY_PER_PAGE);
?>
<div class="container">
    <div class="row row-45 row-md-60">
        <div class="col-sm-12">
            <div class="row row-45 row-md-60 clearleft-custom-5">
                <?php if(!empty($results)) :?>
                <?php foreach($results as $house):?>
                <?php
                if (strpos($house['description'], ".") !== false) {
                    $description = substr($house['description'], 0, strpos($house['description'], '.')) . "...";
                } else {
                    $description = $house['description'];
                }
                ?>
                <div class="col-sm-12 col-md-4">
                    <div class="thumbnail"><a class="img-link" href="<?=Url::to(["houses/view", 'id'=>$house['referenceNumber']]);?>"><img src="<?= ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $house['referenceNumber'];?>/<?= !empty($house['pictures'][0]) ? $house['pictures'][0] : '';?>" alt="<?= $house['titel'];?>" style="width:375px; height:235px"/><span class="thumbnail-price"><?= $house['price'];?> <?= Yii::$app->params['localeCurrency'];?>/<span class="mon"><?= $house['paymentMode'];?></span></span></a>
                        <div class="caption">
                            <h4><a href="<?=Url::to(["houses/view", 'id'=>$house['referenceNumber']]);?>"><?= $house['titel'];?></a></h4>
                            <ul class="describe-1">
                            <?php if(!empty($house['roomSize']) || $house['roomSize'] > 0):?>
                            <li><span class="icon-square"><span class="icon-square">
                                <svg x="0px" y="0px" viewbox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                                    <g>
                                    <path d="M3.6,75.7h3.6V7.3l85.7-0.1v85.3l-16.7-0.1l0-16.7c0-0.9-0.4-1.9-1-2.5c-0.7-0.7-1.6-1-2.5-1h-69V75.7h3.6                            H3.6v3.6H69L69,96c0,2,1.6,3.6,3.6,3.6l23.8,0.1c1,0,1.9-0.4,2.5-1c0.7-0.7,1-1.6,1-2.5V3.6c0-1-0.4-1.9-1-2.5                            c-0.7-0.7-1.6-1-2.5-1L3.6,0.1C1.6,0.2,0,1.7,0,3.7v72c0,0.9,0.4,1.9,1,2.5c0.7,0.7,1.6,1,2.5,1V75.7z"></path>
                                    <path d="M38.1,76.9v-9.5c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v9.5c0,1.3,1.1,2.4,2.4,2.4                            C37,79.3,38.1,78.2,38.1,76.9"></path>
                                    <path d="M38.1,50.7V15c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v35.7c0,1.3,1.1,2.4,2.4,2.4                            C37,53.1,38.1,52.1,38.1,50.7"></path>
                                    <path d="M2.4,38.8h33.3c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H2.4c-1.3,0-2.4,1.1-2.4,2.4                            C0,37.8,1.1,38.8,2.4,38.8"></path>
                                    <path d="M35.7,46h31c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4h-31c-1.3,0-2.4,1.1-2.4,2.4                            C33.3,44.9,34.4,46,35.7,46"></path>
                                    <path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                            C76.2,44.9,77.3,46,78.6,46"></path>
                                    <path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                            C76.2,44.9,77.3,46,78.6,46"></path>
                                    <path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                            C79.9,46,81,44.9,81,43.6"></path>
                                    <path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                            C79.9,46,81,44.9,81,43.6"></path>
                                    </g>
                                </svg></span></span><?= $house['roomSize'];?>m²
                            </li>
                            <?php endif;?>
                                <li><span class="icon icon-sm icon-darker hotel-icon-10"></span><?= ucfirst($house['town']);?></li>
                            </ul>
                            <ul class="describe-2">
                                <li><span class="icon icon-sm icon-darker hotel-icon-05"></span><?= \app\components\Utils::pluralWords( $house['roomCount'], 'Pièce');?></li>
                                <?php //<li><span class="icon icon-sm icon-darker hotel-icon-26"></span>2 garages</li>?>
                            </ul>
                            <p><?= $description;?></p>
                        </div>
                    </div>
                </div>
                    <?php endforeach;?>
                <?php else:?>
                    Aucun résultat trouvé... Veuillez réessayer d'autres options.
                <?php endif;?>
                <?php if($housesCount > FrontendSetting::LIST_PROPERTY_PER_PAGE) :?>
                    <div class="col-sm-12 text-center text-md-left">
                        <ul class="pagination">
                            <?php for ($i=0;$i<$pages;$i++):?>
                            <?php 
                                $url = [
                                    'houses/' . Yii::$app->controller->action->id,
                                    'page'=>$i,
                                    'viewType'=>$viewType
                                ];
                                if(!empty($customParams)) {
                                    $url = array_merge($url, $customParams);
                                }
                            ?>                            
                            <li class="<?=($pageNumber==$i) ? "active" : "";?>"><a href="javascript:;" data-href="<?=Url::to($url);?>" id="results-page_<?=$i+1?>" class="result-pagination-number"><?=$i+1;?></a></li>
                            <?php endfor;?>
                        </ul>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>