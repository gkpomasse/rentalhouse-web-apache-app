<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Nos Logements disponibles';
?>
    <!-- Page Content-->
    <!-- Section Title Breadcrumbs-->
    <section class="section-full section-full-mod-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1><?=Html::encode($this->title);?></h1>
                    <p></p>
                    <ol class="breadcrumb">
                        <li><a href="<?=Url::to(["site/index"]);?>">Acceuil</a></li>
                        <li class="active">Logements</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

<?=Yii::$app->controller->renderPartial('_results', [
    'results' => $results,
    'searchData' => $searchData,
    'searchedParams' => $searchedParams,
    'viewType' => $viewType,
    'searchModel' => $searchModel,
    'housesCount' => $housesCount,
    'pageNumber' => $pageNumber
]);?>