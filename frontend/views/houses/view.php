<?php
    /* @var $this yii\web\View */
    use yii\helpers\Url;
    
    $this->title = 'Détails du Logement';
    
    use common\components\ApiHelper;
    use app\components\Utils;
    //format publication date
    $formatPublicationDate = str_replace(".", "-", $data['publicationDate']);
    $publicationDate = Utils::dateToFrench($formatPublicationDate, 'j F Y');

    if(!empty($data['rentDeposit']) && $data['rentDeposit'] != 0) {
        $nombre_payment_mode = ceil($data['rentDeposit'] / $data['price']);
    }
?>
<!-- Page Content-->
<!-- Section Title Breadcrumbs-->
<section class="section-full">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?=$this->title;?></h1>
                <p></p>
                <ol class="breadcrumb">
                    <li><a href="<?=Url::to(["site/index"]);?>">Acceuil</a></li>
                    <li><a href="<?=Url::to(["houses/index"]);?>">Logements</a></li>
                    <li class="active"><?=$this->title;?></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Section Catalog Single Left-->
<section class="section-sm bg-default">
    <div class="container">
        <h2><?=$data['titel']?></h2>
        <hr>
        <div class="row row-45 row-md-60">
            <div class="col-sm-12 col-lg-7">
                <div class="product-carousel" data-lightbox="gallery">
                    <!-- Slick Carousel-->
                    <div class="slick-slider slider carousel-parent" data-arrows="true" data-loop="false" data-dots="false" data-swipe="true" data-items="1" data-child="#child-carousel" data-for="#child-carousel">
                        <?php foreach ($data['pictures'] as $picture):?>
                            <?php 
                                $picture_link = ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $data['referenceNumber'] . SYS_DIRECTORY_SEPARATOR . $picture;
                            ?>
                            <div class="item">
                                <a href="<?= $picture_link;?>" data-lightbox="image"><img src="<?= $picture_link;?>" alt="" width="770" height="520"></a>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <?php if(count($data['pictures']) > 1) :?>                  
                            <div class="carousel-thumbnail slider slick-slider" id="child-carousel" data-for=".carousel-parent" data-arrows="true" data-loop="false" data-dots="false" data-swipe="false" data-items="3" data-sm-items="3" data-md-items="5" data-lg-items="5" data-xl-items="5" data-slide-to-scroll="1" data-md-vertical="true">
                                <?php foreach ($data['pictures'] as $picture):?>
                                    <?php 
                                        $picture_link = ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $data['referenceNumber'] . SYS_DIRECTORY_SEPARATOR . $picture;
                                ?>                                  
                                <div class="item"><img src="<?= $picture_link;?>" alt="" style="width: 112px; height : 80px"></div>
                                <?php endforeach;?>
                            </div>                        
                    <?php endif;?>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <h4>Description</h4>
                        <p><?=$data['description'];?></p>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr style="background-color: #168c66; color: #FFF">
                                    <th colspan="2">Détails</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Type de Logement</td>
                                        <td><?= ucfirst($data['housingType']);?></td>
                                    </tr>     
                                    <tr>
                                        <td>Nombre de Pièces</td>
                                        <td><?= ucfirst($data['roomCount']);?></td>
                                    </tr>    
                                    <?php if(!empty($data['shower'])):?>
                                    <tr>
                                        <td>Douche Interne</td>
                                        <td><?= ($data['shower'] == 'interne') ? 'Oui' : 'Non';?></td>
                                    </tr>   
                                    <?php endif;?>
                                    <?php if(!empty($data['rentDeposit']) && $data['rentDeposit'] != 0):?>
                                    <tr>
                                        <td>Avance</td>
                                        <td><?=$data['rentDeposit'];?> <?= Yii::$app->params['localeCurrency'];?> (<?= (isset($nombre_payment_mode)) ? $nombre_payment_mode . ' ' . $data['paymentMode'] : '';?>)</td>
                                    </tr>
                                    <?php endif;?>
                                    <?php if(!empty($data['electricityDeposit']) && $data['electricityDeposit'] != 0):?>
                                    <tr>
                                        <td>Caution Electricité</td>
                                        <td><?=$data['electricityDeposit'];?> <?= Yii::$app->params['localeCurrency'];?></td>
                                    </tr>  
                                    <?php endif;?>
                                    <?php if(!empty($data['electricityCounterType'])):?>
                                    <tr>
                                        <td>Type de Compteur</td>
                                        <td><?= ucfirst($data['electricityCounterType']);?></td>
                                    </tr>   
                                    <?php endif;?>
                                    <tr>
                                        <td>Localisation</td>
                                        <td><?=$data['zone'];?>, <?=$data['town'];?></td>
                                    </tr> 
                                    <?php if(!empty($data['additionalInfo'])):?>
                                    <tr>
                                        <td>Informations additionels</td>
                                        <td><?=$data['additionalInfo'];?></td>
                                    </tr>                                  
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-5">
                <div class="catalog-sidebar row row-30">
                    <div class="sidebar-module col-lg-12 text-left order-lg-0">
                        <ul class="list-unstyled small">
                            <li><span class="text-darker">Ajoutée le:</span>
                                <time datetime="2015"><?= $publicationDate;?></time>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-module col-lg-12 order-lg-2">
                        <ul class="describe-1 list-unstyled">
                            <li><span class="icon icon-sm icon-darker material-icons-location_city"></span> <?= ucfirst($data['housingType']);?></li>
                            <li><span class="icon icon-sm icon-darker hotel-icon-05"></span><?= \app\components\Utils::pluralWords( $data['roomCount'], 'Pièce');?></li>                           
                        </ul>
                        <ul class="describe-2 list-unstyled preffix-2">
                            <li>
                                <span class="icon icon-sm icon-darker material-icons-place" style="margin-left: -7px; font-size: 35px;"></span> <?= ucfirst($data['town']);?>
                            </li>        
                            <?php if(!empty($data['roomSize']) || $data['roomSize'] > 0):?>                        
                            <li>
                                <span class="icon-square">
                                    <svg x="0px" y="0px" viewbox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                                      <g>
                                        <path d="M3.6,75.7h3.6V7.3l85.7-0.1v85.3l-16.7-0.1l0-16.7c0-0.9-0.4-1.9-1-2.5c-0.7-0.7-1.6-1-2.5-1h-69V75.7h3.6                      H3.6v3.6H69L69,96c0,2,1.6,3.6,3.6,3.6l23.8,0.1c1,0,1.9-0.4,2.5-1c0.7-0.7,1-1.6,1-2.5V3.6c0-1-0.4-1.9-1-2.5                      c-0.7-0.7-1.6-1-2.5-1L3.6,0.1C1.6,0.2,0,1.7,0,3.7v72c0,0.9,0.4,1.9,1,2.5c0.7,0.7,1.6,1,2.5,1V75.7z"></path>
                                        <path d="M38.1,76.9v-9.5c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v9.5c0,1.3,1.1,2.4,2.4,2.4                      C37,79.3,38.1,78.2,38.1,76.9"></path>
                                        <path d="M38.1,50.7V15c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v35.7c0,1.3,1.1,2.4,2.4,2.4                      C37,53.1,38.1,52.1,38.1,50.7"></path>
                                        <path d="M2.4,38.8h33.3c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H2.4c-1.3,0-2.4,1.1-2.4,2.4                      C0,37.8,1.1,38.8,2.4,38.8"></path>
                                        <path d="M35.7,46h31c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4h-31c-1.3,0-2.4,1.1-2.4,2.4                      C33.3,44.9,34.4,46,35.7,46"></path>
                                        <path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                      C76.2,44.9,77.3,46,78.6,46"></path>
                                        <path d="M78.6,46h16.7c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4H78.6c-1.3,0-2.4,1.1-2.4,2.4                      C76.2,44.9,77.3,46,78.6,46"></path>
                                        <path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                      C79.9,46,81,44.9,81,43.6"></path>
                                        <path d="M81,43.6v-7.1c0-1.3-1.1-2.4-2.4-2.4c-1.3,0-2.4,1.1-2.4,2.4v7.1c0,1.3,1.1,2.4,2.4,2.4                      C79.9,46,81,44.9,81,43.6"></path>
                                      </g>
                                    </svg>
                                </span>                                
                                <?= $data['roomSize'];?>m²
                            </li>    
                            <?php endif;?>                        
                        </ul>
                    </div>
                    <div class="sidebar-module col-md-6 col-lg-12 order-lg-5">
                        <?php /*<!-- RD Google Map-->
                        <div class="rd-google-map rd-google-map__model rd-google-map-mod-1" data-marker="images/gmap_marker.png" data-marker-active="images/gmap_marker_active.png" data-styles="[{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#444444&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.business&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;labels.icon&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b4d4e1&quot;},{&quot;visibility&quot;:&quot;on&quot;}]}]" data-zoom="14" data-x="<?=$location['longitude'];?>" data-y="<?=$location['latitude'];?>">
                            <ul class="map_locations">
                                <li data-y="<?=$location['latitude'];?>" data-x="<?=$location['longitude'];?>">
                                    <p><?=$data['zone'];?>, <?=$data['town'];?></p>
                                </li>
                            </ul>
                        </div>*/?>
                    </div>
                    <div class="sidebar-module col-md-6 col-lg-12 order-lg-3">
                        <div class="price">
                            <p class="small">Prix</p>
                            <p>
                                <span class="h4"><?= $data['price'];?> <?= Yii::$app->params['localeCurrency'];?>/<?= $data['paymentMode'];?></span>
                                <?php /*<span class="h6 text-darker"><del>$402.00/day</del></span>*/?>
                            </p>
                            <div class="btn-group-isotope">
                                <a class="btn btn-primary btn-primary-transparent btn-md btn-min-width-lg" href="<?=Url::to(["site/contact"]);?>">Nous Contacter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section Catalog Single Right-->
<section class="section-sm bg-default">
    <div class="container text-center text-lg-left">
        <?php /*<h2>Similar Properties</h2>*/ ?>
        <h2>Logements récents</h2>
        <hr>
        <div class="row row-45 row-md-60 clearleft-custom text-left">
            <?=Yii::$app->controller->renderPartial('/site/_recents', [
                'houses' => $houses,
                'displayConfigurations' => $displayConfigurations
            ]);?>
        </div>
    </div>
</section>