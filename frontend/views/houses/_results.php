<?php
/**
 * Created by PhpStorm.
 * User: Lano
 * Date: 07/04/2018
 * Time: 21:22
 */
use yii\helpers\Url;
use yii\helpers\Html;

use common\components\ApiHelper;
?>

<!--Section Find your home-->
<section class="section-sm section-sm-mod-1 bg-gray-dark">
    <div class="container position-margin-top">
        <div class="search-form-wrap bg-white container-shadow">
            <?=Yii::$app->controller->renderPartial('/site/_search', [
                'data' => $searchData,
                '_formData' => $searchedParams,
                'searchModel' => $searchModel
            ]);?>
        </div>
    </div>
</section>
<?php if(!empty($results)) :?>
<!-- Section Sorting-->
<section class="section-xs" id="<?=(!empty($searchedResult) && $searchedResult) ? "search-results-section" : "normal-results-section"?>">
    <div class="container sort-panel-between" id="filter-results">
        <!-- RD SelectMenu-->
        <div class="sort-input pull-xs-left">
            <label class="small text-regular" for="sort-1">Filtre
                <?php echo Html::dropDownList('listname', 'SORT_BY_DATE', ApiHelper::getHomeResultsFilterOptions(), ['class'=>"rd-mailform-select", 'id' => "sort-results", 'name' => "select"]);?>
            </label>
            <img id="filter-dropdown-loading" style="position: relative;margin-top: -80px;left: 260px;width: 40px;height: 40px;display: none;" src="<?=\yii\helpers\Url::home(true)?>/images/loading.gif"/>
        </div>
        <div class="btn-group-mod-3 sorting text-left pull-xs-right" id="view-type">
            <a class="btn btn-primary btn-sm <?= ($viewType==ApiHelper::LIST_VIEW_MODE) ? 'active' : ''?>" href="javascript:;" data-view-type="<?=ApiHelper::LIST_VIEW_MODE;?>">Liste</a>
            <a class="btn btn-primary btn-sm <?= ($viewType==ApiHelper::GRID_VIEW_MODE) ? 'active' : ''?>" href="javascript:;" data-view-type="<?=ApiHelper::GRID_VIEW_MODE;?>">Grille</a>
        </div>
    </div>
</section>
<?php endif;?>
<!-- Section Catalog-->
<section class="section-sm" id="houses-results" style="padding-top: 25px">
    <?=Yii::$app->controller->renderPartial('_results_list_' . $viewType, [
        'results' => $results,
        'viewType' => $viewType,
        'housesCount' => $housesCount,
        'pageNumber' => $pageNumber,
        'customParams' => (!empty($customParams)) ? $customParams : []
    ]);?>
</section>