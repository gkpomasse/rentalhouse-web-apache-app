<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Nous Contacter';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-wrap .form-group span.form-validation {
        top: 10px;
    }
    .error {
        color: red;
    }
</style>
      <!-- Page Content-->
      <!-- Section Title Breadcrumbs-->
      <section class="section-full">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h1><?= Html::encode($this->title) ?></h1>
              <p></p>
              <ol class="breadcrumb">
                <li><a href="<?=Url::to("acceuil");?>">Acceuil</a></li>
                <li class="active"><?= Html::encode($this->title) ?></li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!--Section Contact Us 1-->
      <section class="section-sm">
        <div class="container">
          <div class="row row-60">
            <div class="col-lg-8">
              <h2>Envoyer une requête</h2>
              <hr>
              <p> Vous pouvez nous contacter à n'importe quel moment. Nous sommes disponible 24h/24 7j/7. N'hesitez donc pas!!!</p>
              <p>Nous serons content de répondre à vos questions.</p>
              <div class="row row-30 row-md-60">
                <div class="col-sm-12 col-md-4">
                  <dl class="contact-info">
                    <dt><span class="h4">Contacts</span></dt>
                    <dd><a href="callto:<?=Yii::$app->params['contactPhone'];?>"><?=Yii::$app->params['contactPhone'];?><br></a><a href="callto:<?=Yii::$app->params['contactPhone3'];?>"><?=Yii::$app->params['contactPhone3'];?></a></dd>
                  </dl>
                </div>
                <div class="col-sm-12 col-md-4">
                  <dl class="contact-info">
                    <dt><span class="h4">E-mail</span></dt>
                    <dd><a class="text-accent" href="mailto:<?=Yii::$app->params['contactEmail'];?>"><?=Yii::$app->params['contactEmail'];?></a></dd>
                  </dl>
                </div>
                <div class="col-sm-12">
                  <h4>Formulaire</h4>
                  <!-- RD Mailform-->
                    <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => []]); ?>
                        <div class="row row-20">
                            <div class="col-md-6">
                                  <div class="form-wrap">
                                    <?= $form->field($model, 'prenom', [
                                        ])->textInput([ 
                                            "id" => "contact-first-name",
                                            "type" => "text",
                                            "data-constraints" => "@Required",
                                            "class" => "form-input"
                                        ])->label(''); ?>
                                    <label class="form-label" for="contact-first-name">Votre Prénom</label>
                                  </div>
                            </div>
                            <div class="col-md-6 input-mod-1">
                                  <div class="form-wrap">
                                    <?= $form->field($model, 'nom', [
                                        ])->textInput([ 
                                            "id" => "contact-last-name",
                                            "type" => "text",
                                            "data-constraints" => "@Required",
                                            "class" => "form-input"
                                        ])->label(''); ?>                                      
                                    <label class="form-label" for="contact-last-name">Votre nom</label>
                                  </div>
                            </div>
                            <div class="col-12">
                                  <div class="form-wrap">
                                    <?= $form->field($model, 'email', [
                                        ])->textInput([ 
                                            "id" => "login-email-2",
                                            "type" => "email",
                                            "data-constraints" => "@Email @Required",
                                            "class" => "form-input"
                                        ])->label(''); ?>                                         
                                    <label class="form-label" for="login-email-2">Addresse email</label>
                                  </div>
                            </div>
                            <div class="col-12">
                                  <div class="form-wrap">
                                    <label class="form-label" for="contact-message">Votre Message</label>
                                    <?= $form->field($model, 'body', [
                                        ])->textarea([ 
                                            "id" => "contact-message",
                                            "data-constraints" => "@Required",
                                            "class" => "form-input"
                                        ])->label(''); ?>                                      
                                  </div>
                            </div>
                        <div class="col-12">
                            <div class="form-wrap" id="captcha-validation">
                                <div id="recaptcha" class="g-recaptcha" data-sitekey="6LdC7EsUAAAAACLwtVHYsQmNZaE6Y8GyApV95XSM"></div>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="col-12">
                            <?=\yii\bootstrap\Html::submitButton(Yii::t('app', 'Envoyer'), [
                                'class' => 'btn btn-primary btn-md'
                            ]);?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="row row-45 sidebar">
                <div class="col-sm-12 col-md-4 col-lg-12">
                  <h4>Nous Suivre</h4>
                  <ul class="list-inline">
                      <li><a class="fa-facebook" href="<?=Yii::$app->params['facebookUrl'];?>" target="_blank"></a></li>
                    <li><a class="fa-twitter" href="<?=Yii::$app->params['twitterUrl'];?>" target="_blank"></a></li>
                  </ul>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-12">
                  <address class="address"><span class="h4">Addresse</span>
                    <p>Tankpè, Abomey-Calavi</p>
                  </address>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-12">
                  <h4>Horaires d'ouverture</h4>
                  <p>8:00 - 18:00 Lundi - Samedi</p>
                </div>
                <div class="col-sm-12">
                  <!-- RD Google Map-->
                  <div class="rd-google-map rd-google-map__model rd-google-map-mod-2" data-marker="images/gmap_marker.png" data-marker-active="images/gmap_marker_active.png" data-styles="[{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#444444&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.business&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;labels.icon&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b4d4e1&quot;},{&quot;visibility&quot;:&quot;on&quot;}]}]" data-zoom="14" data-x="2.3468195" data-y="6.4503024">
                    <ul class="map_locations">
                      <li data-y="6.4503024" data-x="2.3468195">
                        <p>Abomey-Calavi</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>