<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Se Connecter';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #login-form a {
        color: #FFF;
    }
    
    #login-form input[type="checkbox"]{
        width: 25px;
        height: 20px;
        vertical-align: bottom;
    }
</style>    
<!--Section Login-->
<section class="section-height-mod-1 bg-image bg-image-1 section-middle">
    <div class="container z-index">
        <div class="row justify-content-center justify-content-lg-start">
            <div class="col-sm-12 col-md-8 col-xl-5">
                <div class="jumbotron jumbotron-mod-1 text-center text-lg-left">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php 
                        $form = ActiveForm::begin([
                            'id' => 'login-form', 
                            'fieldConfig' => ['template' => "{label}\n{input}\n{hint}"]
                        ]);
                    ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'required' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput(['required' => true]) ?>

                    <?= $form->field($model, 'rememberMe')->checkbox() ?>

                    <div style="color:#999;margin:1em 0">
                        <?= Html::a('Mot de Passe oublié ?', ['site/request-password-reset']) ?>.
                    </div>
                    <div style="color:#999;margin:1em 0">
                        <?= Html::a('Pas de Compte ?', ['site/registration']) ?>.
                    </div>                    

                    <div class="form-group">
                        <?= Html::submitButton('Connexion', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
