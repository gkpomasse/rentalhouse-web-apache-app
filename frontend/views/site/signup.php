<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Inscription';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section-height-mod-1 bg-image bg-image-1 section-middle">
    <div class="container z-index">
        <div class="row justify-content-center justify-content-lg-start">
            <div class="col-md-8">
                <div class="jumbotron jumbotron-mod-1 text-center text-lg-left">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                    <h1><?= Html::encode($this->title) ?></h1>

                    <div class="row" style="margin:0;padding:0">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'surName')->textInput(['autofocus' => true, 'required' => true, 'placeholder' => 'Votre nom...']) ?>                           
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'firstName')->textInput(['required' => true, 'placeholder' => 'Votre prénom...']) ?>
                        </div>
                    </div>
                    <div class="row" style="margin:0;padding:0">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'telephoneNr')->textInput(['required' => true, 'placeholder' => 'Votre numéro']) ?>                         
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'email')->textInput(['required' => true, 'type' => 'email', 'placeholder' => 'Votre adresse Email...']) ?>
                        </div>
                    </div>     
                    <div class="row" style="margin:0;padding:0">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'password')->passwordInput(['required' => true, 'placeholder' => 'Votre mot de passe...']) ?>                       
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'confirmationPassword')->passwordInput(['required' => true, 'placeholder' => 'Confirmation du mot de passe...']) ?>
                        </div>
                    </div>                     
                    <div class="form-group" style="margin-left:14px;">
                        <?= Html::submitButton('Je m\'inscris', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>        
    </div>
</section>