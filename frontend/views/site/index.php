<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\components\ApiHelper;
use app\components\FrontendSetting;

$this->title = Yii::$app->name . ' : Trouver un logement en quelques clics';

$sliderHouses = array_slice($houses, 0, FrontendSetting::HOMEPAGE_SLIDER_MAX);

$perRow = FrontendSetting::HOMEPAGE_CATEGORY_PER_ROW_MAX;
$housingTypeOptions = array_values(array_filter($data['housingTypeOptions'], function($item) use ($housingTypeCounts) {
  $count = (!empty($housingTypeCounts[$item['key']])) ? $housingTypeCounts[$item['key']] : 0;
  return ($count > 0);
}));

$totalCategories = count($housingTypeOptions);

$numberCategoriesRows = ceil($totalCategories / $perRow);

$HousingTypepictures = ApiHelper::categoryPictures();
?>
      <section>
        <!--Swiper-->
        <div class="swiper-container swiper-slider" data-height="" data-min-height="600px">
          <div class="swiper-wrapper">
              <?php foreach ($sliderHouses as $sliderHouse) :?>
              <div class="swiper-slide" data-slide-bg="<?= ApiHelper::HOST_URL . ApiHelper::HOUSE_UPLOAD_PATH . SYS_DIRECTORY_SEPARATOR . $sliderHouse['referenceNumber'];?>/<?= !empty($sliderHouse['pictures'][0]) ? rawurlencode($sliderHouse['pictures'][0]) : '';?>">
                      <div class="swiper-slide-caption">
                        <div class="container">
                          <p class="h1" data-caption-animate="fadeInDown"><?= $sliderHouse['titel'];?></p>
                          <p class="h5" data-caption-animate="fadeInUp" data-caption-delay="400"><?= $sliderHouse['description'];?></p>
                          <div class="price" data-caption-animate="fadeInUp" data-caption-delay="400"><?= $sliderHouse['price'];?> <?= Yii::$app->params['localeCurrency'];?><span>/day</span></div><a class="btn btn-sm btn-primary" href="<?= Url::to(["houses/view", 'id'=>$sliderHouse['referenceNumber']]);?>" data-caption-animate="fadeInUp" data-caption-delay="400">Consulter</a>
                        </div>
                      </div>
                    </div>
            <?php endforeach;?>
          </div>
          <!-- Swiper Pagination-->
          <div class="swiper-pagination"></div>
        </div>
      </section>
      <!-- Page Content-->
      <!--Section Find your home-->
      <section class="section-sm section-sm-mod-1 bg-gray-dark">
        <div class="container position-margin-top">
          <div class="search-form-wrap bg-white container-shadow">
            <?=Yii::$app->controller->renderPartial('_search', [
                'data' => $data,
                'searchModel' => $searchModel
            ]);?>
          </div>
        </div>
      </section>
      <!--Section Categories-->
      <section class="section-sm section-sm-mod-3 bg-gray-dark text-center text-md-left">
        <div class="container">
          <h2>Catégories</h2>
          <hr>
          <div class="row row-30 row-margin-clear justify-content-center text-left">
            <?php $colx=0;?>
            <?php for ($row=0; $row<$numberCategoriesRows; $row++) :?>
                <div class="row-margin-clear row row-30 col-md-12 col-lg-12 order-lg-1">
                    <?php for ($col=0; $col<$perRow; $col++) :?>
                        <?php if($colx < $totalCategories):?>
                        <?php
                            $cel = $colx;
                            $housingTypeValue = (!empty($housingTypeOptions[$cel])) ? $housingTypeOptions[$cel]['value'] : '';
                            $housingTypeKey = (!empty($housingTypeOptions[$cel])) ? $housingTypeOptions[$cel]['key'] : '';
                            $housingTypeCount = (!empty($housingTypeCounts[$housingTypeKey])) ? $housingTypeCounts[$housingTypeKey] : 0;
                        ?>
                            <div class="category wow fadeInUp col-md-<?=ceil(12/$perRow)?>" style="margin-top: 0; max-width: 500px; max-height: 200px" data-wow-delay=".3s"><img src="<?=(!empty($HousingTypepictures[$cel])) ? $HousingTypepictures[$cel] : 'images/index-1.jpg'?>" alt="" width="370" height="200"/>
                                <div class="category-content">
                                    <h4><?=strtoupper($housingTypeValue);?></h4>
                                    <p><?=$housingTypeCount?> logements disponibles</p><a class="btn btn-sm btn-primary" href="<?= Url::to(["houses/category", 'id'=>$housingTypeKey]);?>">Plus de détails</a>
                                </div>
                            </div>
                        <?php $colx++;?>
                        <?php endif;?>
                    <?php endfor;?>                 
                </div>
            <?php endfor;?>
          </div>
        </div>
      </section>
      <!--Section Recent Properties-->
      <section class="section-md text-center text-md-left">
        <div class="container">
          <h2>LOGEMENTS RECENTS</h2>
          <hr>
          <div class="row row-45 row-md-60 clearleft-custom">
              <?=Yii::$app->controller->renderPartial('_recents', [
                  'houses' => $houses,
                  'displayConfigurations' => $displayConfigurations
              ]);?>
          </div>
        <a class="btn btn-md btn-primary" href="<?= Url::to(["houses/index"]);?>">Tous les logements</a>
        </div>
      </section>
      <!--Section Our Advantages-->
     <?php /* <section class="section-md bg-gray text-center text-lg-left">
        <div class="container">
          <h2>Our Advantages</h2>
          <hr>
          <div class="row row-60 clearleft-custom-2">
            <div class="col-md-6 col-lg-6 wow fadeInUp" data-wow-delay=".2s">
              <div class="media media-mod-1 flex-column align-items-center flex-lg-row align-items-lg-start">
                <div class="media-left"><span class="linecons-location4 icon icon-lg bg-primary"></span></div>
                <div class="media-body">
                  <h4>Various Locations</h4>
                  <p>Search by state then by city to find an apartment  overlooking the lake in Chicago, within walking distance of the beach in Los Angeles or in the heart of Atlanta.</p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 wow fadeInUp" data-wow-delay=".4s">
              <div class="media media-mod-1 flex-column align-items-center flex-lg-row align-items-lg-start">
                <div class="media-left"><span class="linecons-camera7 icon icon-lg bg-primary"></span></div>
                <div class="media-body">
                  <h4>View Apartments</h4>
                  <p>View apartment listings with photos, HD videos, InsideView virtual tours, 3D floor plans, and more, while also choosing the apartment and community features that you want.</p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 wow fadeInUp" data-wow-delay=".6s">
              <div class="media media-mod-1 flex-column align-items-center flex-lg-row align-items-lg-start">
                <div class="media-left"><span class="linecons-blockade icon icon-lg bg-primary"></span></div>
                <div class="media-body">
                  <h4>Privacy and Security</h4>
                  <p>Renters insurance helps keep your belongings secure, whether they're on your desk, under your couch, or in some cases, even in your car's glove box.</p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 wow fadeInUp" data-wow-delay=".8s">
              <div class="media media-mod-1 flex-column align-items-center flex-lg-row align-items-lg-start">
                <div class="media-left"><span class="linecons-banknote icon icon-lg bg-primary"></span></div>
                <div class="media-body">
                  <h4>No Commission</h4>
                  <p>You will therefore appreciate this opportunity to acquire a high-quality apartment for rent without having to pay any commission to our real estate agency.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Section Our Team-->
      <section>
        <!-- Owl Carousel-->
        <div class="owl-carousel" data-items="1" data-sm-items="2" data-lg-items="3" data-xl-items="4" data-stage-padding="0" data-loop="false" data-margin="0" data-dots="false" data-nav="true" data-autoplay="true">
          <div class="owl-item"><img src="images/index-12.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Michelle Nelson</h4>
              <p>Realtor</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
          <div class="owl-item"><img src="images/index-13.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Brandon Miller</h4>
              <p>Manager</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
          <div class="owl-item"><img src="images/index-14.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Christina Harvey</h4>
              <p>Realtor</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
          <div class="owl-item"><img src="images/index-15.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Carolyn Stone</h4>
              <p>Realtor</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
          <div class="owl-item"><img src="images/index-12.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Michelle Nelson</h4>
              <p>Realtor</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
          <div class="owl-item"><img src="images/index-13.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Brandon Miller</h4>
              <p>Manager</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
          <div class="owl-item"><img src="images/index-14.jpg" alt="" width="480" height="570">
            <div class="owl-item-content">
              <h4>Christina Harvey</h4>
              <p>Realtor</p><a class="btn btn-sm btn-primary" href="agent-personal.html">get in touch</a>
            </div>
          </div>
        </div>
      </section>
      <!--Section Testimonial-->
      <section class="section-md text-center text-md-left">
        <div class="container">
          <h2> Testimonials</h2>
          <hr>
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-carousel-mod-2" data-items="1" data-md-items="2" data-lg-items="3" data-loop="false" data-margin="30" data-dots="true" data-nav="false" data-autoplay="true">
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-16.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Laura Russell</cite>
                <p>
                  <q>Thanks a lot for the quick response. I was really impressed, your solution is excellent!! Your competence is justified!!!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-17.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Richard Santos</cite>
                <p>
                  <q>I just don't know how to describe your services... They are extraordinary! I am quite happy with them! Just keep up going this way!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-18.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Dylan Medina</cite>
                <p>
                  <q>Thank you very much. I'm impressed with your service. I've already told my friends about your company and your quick response, thanks again!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-16.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Laura Russell</cite>
                <p>
                  <q>Thanks a lot for the quick response. I was really impressed, your solution is excellent!! Your competence is justified!!!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-17.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Richard Santos</cite>
                <p>
                  <q>I just don't know how to describe your services... They are extraordinary! I am quite happy with them! Just keep up going this way!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-18.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Dylan Medina</cite>
                <p>
                  <q>Thank you very much. I'm impressed with your service. I've already told my friends about your company and your quick response, thanks again!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-16.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Laura Russell</cite>
                <p>
                  <q>Thanks a lot for the quick response. I was really impressed, your solution is excellent!! Your competence is justified!!!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-17.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Richard Santos</cite>
                <p>
                  <q>I just don't know how to describe your services... They are extraordinary! I am quite happy with them! Just keep up going this way!</q>
                </p>
              </blockquote>
            </div>
            <div class="owl-item">
              <blockquote class="quote">
                <div class="img-wrap"><img src="images/index-18.jpg" alt="" width="50" height="50"></div>
                <cite class="h6">Dylan Medina</cite>
                <p>
                  <q>Thank you very much. I'm impressed with your service. I've already told my friends about your company and your quick response, thanks again!</q>
                </p>
              </blockquote>
            </div>
          </div>
        </div>
      </section>
	  */ ?>