<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>
<!--Section 404-->
<section class="section-height section-height-1 bg-image bg-image-1 section-middle">
  <div class="container z-index">
    <div class="jumbotron jumbotron-mod-1 text-center text-md-left">
      <h1><?= Html::encode($this->title) ?></h1>
          <div class="alert alert-danger">
              <?= nl2br(Html::encode($message)) ?>
          </div>
      <a class="btn btn-transparent btn-md" href="<?=Url::to(['site/index']);?>">Revenir à l'acceuil</a>
    </div>
  </div>
</section>