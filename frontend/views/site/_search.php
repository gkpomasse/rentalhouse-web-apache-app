<?php
/**
 * @author Ghismaon KPOMASSE.
 * Date: 01/04/2018
 * Time: 21:22
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$housingTypes = $data['housingTypeOptions'];//type de logement
$rowsCountOptions = $data['rowsCountOptions'];//nbre de pièces
$townOptions = $data['townOptions'];//town
$rowsZoneOptions = $data['rowsZoneOptions'];//zone
$rowsRadiusOptions = $data['rowsRadiusOptions'];//rayon
$rowspublicationOptions = $data['rowspublicationOptions'];//publications date

?>
<h3><?= \Yii::t('app', "Chercher une Maison / Appartement");?></h3>

<?php $form = ActiveForm::begin([
    'id' => "form-house-search",
    'action' => Yii::$app->urlManager->createUrl("houses/search"),
    'options' => [
        'class' => "form-variant-1",
    ]
]); ?>
<div class="row">
    <div class="col-md-6">
        <label for="select-1"><?= \Yii::t('app', "TYPE DE LOGEMENT")?></label>
        <?= Html::activeDropDownList($searchModel, 'housingType', ArrayHelper::map($housingTypes, 'key', function($item) {return ucfirst($item['value']);}), [
            'prompt'=>'Toutes les offres', 
            'id'=>'select-1',
            'data-selected'=>$searchModel->housingType,
            'class' => 'rd-mailform-select'
        ]) ?>        
    </div>
    <div class="col-md-6">
        <label for="select-2"><?= \Yii::t('app', "TAILLE")?></label>
        <?= Html::activeDropDownList($searchModel, 'roomCount', ArrayHelper::map($rowsCountOptions, 'key', function($item) {return ucfirst($item['value']);}), [
            'prompt'=>'Nombre de pièces', 
            'id'=>'select-2',
            'data-selected'=>$searchModel->roomCount,
            'class' => 'rd-mailform-select'
        ]) ?>         
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label for="select-3"><?= \Yii::t('app', "VILLE")?></label>
        <?= Html::activeDropDownList($searchModel, 'town', ArrayHelper::map($townOptions, 'key', function($item) {return ucfirst($item['value']);}), [
            'prompt'=>'Toutes les villes', 
            'id'=>'select-3',
            'data-selected'=>$searchModel->town,
            'class' => 'rd-mailform-select'
        ]) ?>        
    </div>
    <div class="col-md-3">
        <label for="select-4"><?= \Yii::t('app', "ZONE")?></label>
        <?= Html::activeDropDownList($searchModel, 'zone', ArrayHelper::map($rowsZoneOptions, 'key', function($item) {return ucfirst($item['value']);}), [
            'prompt'=>'Toutes les zones', 
            'id'=>'select-4',
            'data-selected'=>$searchModel->zone,
            'class' => 'rd-mailform-select'
        ]) ?>         
    </div>
    <div class="col-md-3">
        <label for="select-6"><?= \Yii::t('app', "RAYON")?></label>
        <?= Html::activeDropDownList($searchModel, 'radius', ArrayHelper::map($rowsRadiusOptions, 'key', function($item) {return ucfirst($item['value']);}), [
            'prompt'=>'Dans un rayon de', 
            'id'=>'select-6',
            'data-selected'=>$searchModel->radius,
            'class' => 'rd-mailform-select'
        ]) ?>         
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group width-1" style="width: 100%">
            <label for="range-1"><?= \Yii::t('app', "PRIX (FCFA)")?><br></label>
            <?= Html::activeTextInput($searchModel, 'minPrice', ['id'=>"range-1", 'class' => 'rd-range-input-value rd-range-input-value-1']);?>
            <?= Html::activeTextInput($searchModel, 'maxPrice', ['id'=>"range-2", 'class' => 'rd-range-input-value rd-range-input-value-2']);?>
            <div class="rd-range" data-min="5000" data-max="200000" data-start="[5000, 200000]" data-step="500" data-tooltip="true" data-min-diff="100" data-input=".rd-range-input-value-1" data-input-2=".rd-range-input-value-2"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group width-1" style="width: 100%">
            <label for="range-3"><?= \Yii::t('app', "SURFACE (m²)")?><br></label>
            <?= Html::activeTextInput($searchModel, 'minSurface', ['id'=>"range-3", 'class' => 'rd-range-input-value rd-range-input-value-3']);?>
            <?= Html::activeTextInput($searchModel, 'maxSurface', ['id'=>"range-4", 'class' => 'rd-range-input-value rd-range-input-value-4']);?>
            <div class="rd-range" data-min="5" data-max="1000" data-start="[5, 1000]" data-step="5" data-tooltip="true" data-min-diff="2" data-input=".rd-range-input-value-3" data-input-2=".rd-range-input-value-4"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group width-2" style="width: 100%">
            <label for="select-8"><?= \Yii::t('app', "MOTS CLES")?></label>
            <?= Html::activeTextInput($searchModel, 'keyword', ["id"=>"keyword", "placeholder" => "Sanitaire, Meublé ..."]);?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group width-2" style="width: 100%">
            <label for="select-5"><?= \Yii::t('app', "DATE DE PUBLICATION")?></label>
            <?= Html::activeDropDownList($searchModel, 'publicationDate', ArrayHelper::map($rowspublicationOptions, 'key', function($item) {return ucfirst($item['value']);}), [
                'id'=>'select-5',
                'data-selected'=>$searchModel->publicationDate,
                'class' => 'rd-mailform-select',
                'data-calendar' => ''
            ]) ?>             
        </div>
    </div>
</div>
<div class="row">
    <button class="btn btn-md btn-primary"><?= \Yii::t('app', "Rechercher")?></button>
</div>
<?php ActiveForm::end(); ?>