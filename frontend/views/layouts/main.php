<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

$baseUrl = \yii\helpers\Url::home(true);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,700%7CMontserrat:400,700">
    <!--link rel="stylesheet" href="css/bootstrap.css"-->
    <!--link rel="stylesheet" href="css/style.css"-->
    <?= Html::csrfMetaTags() ?>
    <!-- Site Title-->
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
    <style>
        @media (min-width: 1200px) {
            .form-variant-1 .form-group.width-1 input + input {
                margin-left: 56px;
            }
            ul.rd-navbar-nav {
                width: 860px;
            }
            ul.rd-navbar-nav li:first-child {
                margin-left: 400px;
            }
            .header_corporate {
                height: 238.547px;
            }
            .header_corporate .rd-navbar-static .rd-navbar-inner {
                padding-top: 10px;
            }
            .header_corporate .rd-navbar-static .rd-navbar-nav-wrap{
                padding-top: 0;
                padding-bottom: 0;
                margin-bottom: 0;
            }
            .rd-navbar-static .rd-navbar-panel {
                padding-bottom: 10px;
            }
            .rd-navbar-static .btn-group {
                margin-top: 32px;
            }
            .page-foot {
                padding-top: 15px;
                padding-bottom: 20px;
            }
            .page-foot img {
                width: 230px;
                height: 50px;
            }
            .thumbnail .describe-1, .thumbnail .describe-2 + * {
                margin-top: 5px;
            }
            div.caption p {
                font-size: 15px;
            }
        }
        
        #recent-property img {
            height: 250px;
        }
        .ui-pnotify-text {
            text-align: center;
        }
        
        .help-block-error {
            color: red;
            margin: 0
        }
        .row-margin-clear {
            margin-top: 0;
            margin-bottom: 0
        }
    </style>
    <script type="text/javascript">
        var jsBaseUrl = "<?=$baseUrl;?>";
    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134310072-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134310072-1');
</script>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Page-->
<div class="page text-left">
      <!-- Page Header-->
      <header class="page-head">
          <div class="rd-navbar-wrap header_corporate">
              <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-lg-stick-up-offset="160px" data-xl-stick-up-offset="160px" data-xxl-stick-up-offset="160px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true" data-lg-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static">
                  <!--RD Navbar Panel-->
                    <div class="rd-navbar-top-panel">
                        <div class="rd-navbar-top-panel-wrap">
                            <div class="login">
                                <?php /*if(Yii::$app->user->isGuest):?>
                                    <a href="<?=Url::to(['site/login']);?>"><span class="fa-user"></span> Se Connecter</a> &nbsp;&nbsp;
                                    <a href="<?=Url::to(['site/registration']);?>"><span class="fa-user"></span> S'inscrire</a>
                                <?php else:?>
                                    <a href="<?=Url::to(['site/account']);?>" data-method='post'><span class="fa-user"></span> Mon compte</a> &nbsp;&nbsp;
                                    <a href="<?=Url::to(['site/logout']);?>" data-method='post'><span class="fa-times-circle"></span> Déconnexion</a>
                                <?php endif*/;?>
                            </div>
                            <div class="contact-info" style="margin-left:0"><a href="<?php if(!Yii::$app->devicedetect->isMobile()) :?>skype:<?=Yii::$app->params['contactPhone'];?>?call<?php else:?>tel:<?=Yii::$app->params['contactPhone'];?><?php endif;?>"><span class="material-icons-local_phone"></span> <?=Yii::$app->params['contactPhone2'];?></a><a href="mailto:<?=Yii::$app->params['contactEmail'];?>"><span class="material-icons-drafts"></span> <?=Yii::$app->params['contactEmail'];?></a></div>
                            <div class="slogan">
                              <p>Professional realtor works for you</p>
                            </div>
                            <ul class="list-inline">
                              <li><a class="fa-facebook" target="_blank" href="<?=Yii::$app->params['facebookUrl'];?>"></a></li>
                              <li><a class="fa-twitter" target="_blank" href="<?=Yii::$app->params['twitterUrl'];?>"></a></li>
                              <?php if(!Yii::$app->user->isGuest && !empty(Yii::$app->user->identity)):?>
                                <li>Salut, <?=Yii::$app->user->identity->surName;?> <?=Yii::$app->user->identity->firstName;?></li>
                              <?php endif;?>
                            </ul>
                            <div class="btn-group"><a class="btn btn-sm btn-primary" href="<?= Url::to(["houses/create"]);?>">Soumettre un logement</a></div>
                        </div>
                    </div>
                  <div class="rd-navbar-inner inner-wrap">
                      <div class="rd-navbar-panel">
                          <!-- RD Navbar Toggle-->
                          <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                          <!-- RD Navbar Brand-->
                          <div class="rd-navbar-brand"><a class="brand-name" href="<?= Url::to(["site/index"]);?>"><img class="logo-desktop" src="images/logo.png" width="139" height="37" alt=""><img class="logo-mobile" src="images/logo.png" style="width: 139px; height: 37px" alt=""></a></div>
                      </div>
                      <?php /*
                      <div class="btn-group"><a class="btn btn-sm btn-primary" href="<?= Url::to(["houses/create"]);?>">Soumettre un logement</a></div>
                      */
                      ?>
                      <div class="rd-navbar-nav-wrap">
                          <!-- RD Navbar Nav-->
                          <ul class="rd-navbar-nav">
                              <li class="active"><a href="<?= Url::to(["site/index"]);?>">Acceuil</a></li>
                              <li><a href="<?=Url::to(["houses/index"]);?>">LOGEMENTS</a></li>
                              <li><a href="<?=Url::to(["site/contact"]);?>">Nous Contacter</a></li>
                              <li class="link-group">
                                  <?php if(!Yii::$app->devicedetect->isMobile()) :?>
                                      <a href="skype:<?=Yii::$app->params['contactPhone'];?>?call">
                                          <span class="icon icon-xxs-mod-1 material-icons-local_phone"></span> <?=Yii::$app->params['contactPhone2'];?>
                                      </a>
                                  <?php else:?>
                                      <a href="tel:<?=Yii::$app->params['contactPhone'];?>">
                                          <span class="icon icon-xxs-mod-1 material-icons-local_phone"></span> <?=Yii::$app->params['contactPhone2'];?>
                                      </a>
                                  <?php endif;?>
                                  <a href="mailto:#">
                                      <span class="icon icon-xxs-mod-1 material-icons-drafts"></span> <?=Yii::$app->params['contactEmail'];?>
                                  </a>
                                  <?php /*
                                  <a href="<?=Url::to(['site/login']);?>">
                                      <span class="icon icon-xxs-mod-1 fa-user"></span> Se Connecter/S'inscrire
                                  </a>
                                  <a class="btn btn-sm btn-primary" style="margin-top: 120px" href="<?=Url::to("/property/add/");?>">
                                      Soumettre un logement
                                  </a>*/?>
                              </li>
                          </ul>
                      </div>
                  </div>
              </nav>
          </div>
      </header>
    <!-- Page Content -->
    <?= $content ?>
    <!-- Page Footer-->
    <footer class="page-foot text-center text-lg-left bg-gray">
        <div class="container-fluid">
            <div class="footer-wrap">
                <div class="rd-navbar-brand wow fadeInUp" data-wow-delay=".2s"><a class="brand-name" href="<?= Url::to(["site/index"]);?>"><img src="images/logo.png" width="139" height="25" alt=""></a></div>
                <ul class="list-inline wow fadeInUp" data-wow-delay=".4s">
                    <li><a class="fa-facebook" target="_blank" href="<?=Yii::$app->params['facebookUrl'];?>"></a></li>
                    <li><a class="fa-twitter" target="_blank" href="<?=Yii::$app->params['twitterUrl'];?>"></a></li>
                </ul>
                <div class="copyright wow fadeInUp" data-wow-delay=".6s">
                    <p>&#169;<span id="copyright-year"></span> Tous droits réservés <?= date('Y') ?>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<script type="application/javascript">
    $(document).on('change', 'select', function () {
        var parent = $(this).closest('div');
        var currentValue = $(this).find("option:selected").text();
        parent.find('span.rd-select-value').html(currentValue);
        if(parent.find('select').attr('id') == 'sort-results') {
            window.houses.getHousesBySort($(this).val());
        }
    });
    //
    $(document).on('click touchstart', '#filter-results .rd-select-option, #form-house-search .rd-select-option', function (e) {
        var parent = $(this).closest('.rd-select');
        var currentValue = $(this).text();
        parent.find('.rd-select-option').removeClass('selected');
        $(this).addClass('selected');
        parent.find('span.rd-select-value').html(currentValue);
        parent.find('select option').removeAttr('selected');
        var currentOption = parent.find('select option').filter(function() {
            return $(this).text() == currentValue;
        });
        currentOption.attr('selected', true);
        if(parent.find('select').attr('id') == 'sort-results') {//hack way!!!
            var sortingValue = currentOption.attr('value');
            window.houses.getHousesBySort(sortingValue);
        }
    });
    //
    $(document).on('click', '#normal-results-section .sorting a', function () {
        if(!$(this).hasClass('active')) {
            var url = window.location.href;
            url = window.global.removeParam(url, 'viewType');
            window.location.href = window.global.addParam(url, 'viewType', $(this).attr('data-view-type'));
        }
    });
    $(document).on('click', '#search-results-section .sorting a', function () {
        if(!$(this).hasClass('active')) {
            var currentSearchUrl = $('#form-house-search').attr('action');
            currentSearchUrl = window.global.removeParam(currentSearchUrl, 'viewType');
            currentSearchUrl = window.global.addParam(currentSearchUrl, 'viewType', $(this).attr('data-view-type'));
            $('#form-house-search').attr('action', currentSearchUrl);
            $("#form-house-search").submit();
        }   
    });    
    $(document).on('click', '.pagination .result-pagination-number', function() {
        if(!$(this).closest('li').hasClass('active')) {
            $('.pagination li').removeClass('active');
            $(this).closest('li').addClass('active');
            var url = $(this).attr('data-href');
            url = window.global.addParam(url, 'sortBy', $("#sort-results").val());
            url = window.global.addParam(url, 'category', window.global.getParam('id'));
            window.houses.load(url);     
        }
    });
</script>
<?php $this->endBody() ?>
<script type="text/javascript">
    PNotify.prototype.options.width = "400px";
</script>
<?php if(Yii::$app->session->getFlash('error') !== null) :?>
    <script type="text/javascript">
        new PNotify({
            title: '',
            text: "<?=Yii::$app->session->getFlash('error');?>",
            type: 'error',
        });
    </script>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('success') !== null) :?>
    <script type="text/javascript">
        new PNotify({
            title: '',
            text: "<?=Yii::$app->session->getFlash('success');?>",
            type: 'success',
        });
    </script>
<?php endif;?>    
<?php if(Yii::$app->session->getFlash('notice') !== null) :?>
    <script type="text/javascript">
        new PNotify({
            title: '',
            text: "<?=Yii::$app->session->getFlash('notice');?>",
            minHeight: '16px'
        });
    </script>
<?php endif;?>   
</body>
</html>
<?php $this->endPage() ?>
